pub(crate) mod species_amounts;

use std::convert::FloatToInt;
use std::ops::{DivAssign, Mul};
use delegate::delegate;
use crate::settings::Vicsek;

pub(crate) fn derive_precise_float_struct<T,O>(start: T, end: T, step_size: T, precision: u8) -> PreciseFloat<O>
    where
        T: From<O> + Mul<Output = T> + From<u32> + FloatToInt<O>,
{
    let prec = 10u32.pow(u32::from(precision));

    PreciseFloat {
        start: unsafe { (start * T::from(prec)).to_int_unchecked() },
        end: unsafe { (end * T::from(prec)).to_int_unchecked() },
        step_size: unsafe { (step_size * T::from(prec)).to_int_unchecked() },
        precision
    }
}

pub(crate) fn derive_precise_float<T, O>(floating_point: T, precision: u8) -> O
where
    T: From<O> + Mul<Output = T> + From<u32> + FloatToInt<O>
{
    let prec = 10u32.pow(u32::from(precision));

    unsafe { (floating_point * T::from(prec)).to_int_unchecked() }
}

#[derive(Default, Clone)]
pub(crate) struct PreciseFloat<T> {
    pub(crate) start: T,
    pub(crate) end: T,
    pub(crate) step_size: T,
    pub(crate) precision: u8
}

pub(crate) fn convert_with_precision<T, O>(i: T, precision: u8) -> O
    where
        O: From<T> + From<u32> + DivAssign
{
    let mut input = O::from(i);
    let prec = 10u32.pow(u32::from(precision));

    input /= O::from(prec);
    input
}



#[derive(Default)]
pub struct ConfigCollection(Vec<Vicsek>);

impl ConfigCollection {
    delegate! {
        to self.0 {
            pub fn push(&mut self, val: Vicsek);
        }
    }
}

impl FromIterator<Vicsek> for ConfigCollection {
    fn from_iter<T: IntoIterator<Item=Vicsek>>(iter: T) -> Self {
        let mut c = ConfigCollection::default();

        for config in iter {
            c.push(config);
        }
        c
    }
}