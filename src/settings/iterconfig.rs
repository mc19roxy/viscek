use std::iter::StepBy;
use std::ops::{RangeInclusive};
use crate::settings::adapters::{convert_with_precision, derive_precise_float};
use crate::settings::adapters::species_amounts::{SpeciesAmount, SubsetSum};
use crate::settings::Vicsek;

pub fn generate_species_amounts<F>(n: usize, number_of_species: usize, filter: F) -> SubsetSum<F>
{
    SubsetSum::new(n, number_of_species, filter)
}

pub fn generate_float_values(start: f64, end: f64, step_size: f64, precision: Option<u8>) -> FloatStepBy
{
    FloatStepBy::new(start, end, step_size, precision)
}

#[derive(Clone)]
pub struct FloatStepBy {
    iter: StepBy<RangeInclusive<i32>>,
    precision: u8
}

impl FloatStepBy {
    pub fn new(start: f64, end: f64, step_size: f64, precision: Option<u8>) -> Self {
        let precision = precision.unwrap_or(2u8);
        let start = derive_precise_float(start, precision);
        let end = derive_precise_float(end, precision);
        let step_size: i32 = derive_precise_float(step_size, precision);

        Self {
            iter: RangeInclusive::new(start, end).step_by(usize::try_from(step_size).unwrap()),
            precision
        }
    }
}

impl Iterator for FloatStepBy {
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|value| {
            convert_with_precision(value, self.precision)
        })
    }
}

pub fn generate_config_species_amounts<I, F>(iter: I, filter: F) -> SpeciesAmount<I, F>
where
I: Iterator<Item=Vicsek> + Clone,
F: Clone
{
    let mut iter = iter;
    let base_config = iter.next();
    SpeciesAmount::new(iter, base_config, filter)
}

#[cfg(test)]
mod tests {
    use std::iter::{once, repeat};
    use crate::settings::iterconfig::{generate_species_amounts};
    use crate::settings::Vicsek;
}