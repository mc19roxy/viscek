use std::iter::{Repeat, zip, repeat};
use std::ops::RangeInclusive;
use itertools::{Itertools, MultiProduct, repeat_n};
use crate::settings::Vicsek;

pub struct SpeciesAmount<I, F>
{
    iter: I,
    base_config: Option<Vicsek>,
    iter2: Repeat<SubsetSum<F>>,
    internal_iter: Option<SubsetSum<F>>
}

impl <I, F> SpeciesAmount<I, F>
    where
        SubsetSum<F>: Clone
{
    pub fn new(iter: I, base_config: Option<Vicsek>, filter: F) -> Self {
        let first_config = base_config.as_ref().unwrap();
        let n = first_config.N;
        let number_of_species = first_config.groups.len();

        let mut iter1 = repeat(SubsetSum::new(n, number_of_species, filter));

        Self {
            iter,
            base_config,
            internal_iter: iter1.next(),
            iter2: iter1,
        }

    }
}

impl <I, F> Iterator for SpeciesAmount<I, F>
    where SubsetSum<F>: Clone,
          I: Iterator<Item = Vicsek>,
          F: FnMut(&[usize]) -> bool
{
    type Item = Vicsek;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(base_config) = &self.base_config {
            if let Some(ref mut iter) = self.internal_iter {
                if let Some(values) = iter.next() {
                    let mut new_config = base_config.clone();
                    zip(new_config.groups.values_mut(), values).for_each(|((amount, _), value)| {
                        *amount = value;
                    });
                    return Some(new_config)
                }
            } else {
                self.base_config = Some(self.next()?);
                self.internal_iter = self.iter2.next();
            }
        }

        None
    }
}

#[derive(Clone)]
pub struct SubsetSum<F>
{
    filter: F,
    iter: MultiProduct<RangeInclusive<usize>>,
    n: usize,
    species_count: usize
}


impl <F> SubsetSum<F>
{
    pub(crate) fn new(total: usize, k: usize, filter: F) -> Self {
        let range = RangeInclusive::new(0, total);

        let iter = repeat_n(range.into_iter(), k)
            .multi_cartesian_product();

        Self {
            iter,
            filter,
            n: total,
            species_count: k
        }
    }
}

impl<F> Iterator for SubsetSum<F>
    where F: FnMut(&[usize]) -> bool
{
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Self::Item> {
        for v in self.iter.by_ref() {
            if v.iter().sum::<usize>() == self.n && (self.filter)(&v) {
                return Some(v)
            }
        }
        None
    }
}

#[cfg(test)]
mod test {
    use crate::settings::adapters::species_amounts::SubsetSum;

    #[test]
    fn test_subsetsum() {
        let s = SubsetSum::new(2048, 2, |v: &[usize]| {
           v[0] % 128 == 0
        });

        let c: Vec<_> = s.collect();

        println!("Collection: {:?}", c);
    }
}