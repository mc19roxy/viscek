use std::error::Error;
use crate::io::{Syncable};

#[derive(Debug)]
pub enum SimulationError {

}

pub trait Sim<T> {
    fn step(&mut self) -> Result<T, Box<dyn Error>>;
    fn render(&self);
    fn reset(&mut self);
    fn get_obs(&self) -> T;
}

pub trait SimMgr<S, T, U> where T: Sim<S>, U: Clone + Syncable {
    fn simulate(&mut self) -> Result<U, Box<dyn Error>>;
}


