#![feature(convert_float_to_int)]
#![feature(step_trait)]

mod vicsek;
mod settings;
mod vis;
mod cli;
mod simulation;
mod io;
mod observers;
mod run;

fn main() {
    cli::run();
}
