use std::cmp::Ordering;
use std::error::Error;
use crate::settings;
use crate::simulation::{Sim, SimMgr};
use ndarray::prelude::*;
use ndarray::{s, Array2, ArrayView1, indices_of, Zip, stack, concatenate};
use ndarray::parallel::par_azip;
use ndarray_rand::{rand, RandomExt};
use ndarray_rand::rand::seq::SliceRandom;
use ndarray_rand::rand_distr::{Uniform};
use std::f64::consts::PI;
use std::iter::zip;
use std::ops::{Add, AddAssign, Range, Sub, SubAssign};
use std::path::Path;
use std::sync::{Arc, Mutex, RwLock};
use ndarray::parallel::prelude::{IntoParallelRefMutIterator, ParallelIterator};
use fxhash::{FxHashMap, FxHashSet};
use crate::settings::{GroupAlignment, Settings, UpdateStrategy};
use std::{mem, thread};
use std::thread::ScopedJoinHandle;
use ndarray_rand::rand::thread_rng;
use itertools::Itertools;
use ringbuf::{HeapRb};
use serde::{Deserialize, Serialize};
use crate::io::{ConsumerHandler, Metric, MetricValues, Run, Syncable};
use delegate::delegate;
use std::collections::hash_map::{Entry, Iter, IterMut, ValuesMut};
use std::fmt::Debug;
use std::fs::{create_dir, create_dir_all, File};
use std::hash::{Hash, Hasher};
use std::io::Write;
use chrono::{DateTime, Utc};
use ndarray::IntoNdProducer;
use mongodb::bson::{doc};
use crate::observers::events::{ArtifactAdded, ExperimentInfo, Failed, HostInfo, ObserverEvents, Started};
use std::str::FromStr;
use ndarray_rand::rand::prelude::IteratorRandom;
use ndarray_rand::rand_distr::num_traits::real::Real;
use crate::run::{ArcEvent, Event, ExperimentOptions, VicsekInfo};
use crate::vis::{Render, RenderParams, species_to_color, VisParams};
use acap::Proximity;
use acap::vp::VpTree;
use acap::NearestNeighbors;

const CONSUMER_BUFFER_SIZE: usize = 10000;
const PRODUCER_BUFFER_SIZE: usize = 500;

impl UpdateStrategy {
    fn spin_update(&self, spin_of_candidates: &[i8], spin: i8, settings: &settings::Vicsek) -> i8 {
        match self {
            UpdateStrategy::MajorityWithThreshold(threshold, ignored_spins) => { update_spin_variable_majority_with_threshold(spin_of_candidates, spin, settings, *threshold, ignored_spins) },
            UpdateStrategy::Majority(ignored_spins) => {update_spin_majority_alt(spin_of_candidates, spin, settings)}
            UpdateStrategy::Minority(to_spin) => {update_spin_minority(spin_of_candidates, *to_spin, spin, settings)}
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct MetaData {
    pub(crate) command: String
}

enum RotationStates {
    Rotating,
    MaxRotationTimeReached,
    Measuring
}

pub struct VicsekMongoSimMgr {
}

impl VicsekMongoSimMgr {
    pub(crate) fn run(sender: crossbeam::channel::Sender<ArcEvent>,
                            hb_sender: crossbeam::channel::Sender<Vec<Metric>>,
                            config: settings::Vicsek,
                            experiment_info: ExperimentInfo,
                            host_info: HostInfo,
                            meta_info: MetaData,
                            _id: Option<i64>,
                            mut vicsek: Vicsek<'_>,
                            ex_options: &ExperimentOptions,
                            vicsek_info: Arc<RwLock<VicsekInfo>>) -> Self {

        let (one_tx, one_rx) = crossbeam::channel::bounded(2);

        sender.send(Arc::new(ObserverEvents::Started(Started {
            ex_info: experiment_info.to_owned(),
            command: "vicsek".to_string(),
            host_info: host_info.to_owned(),
            start_time: Utc::now(),
            config: config.to_owned(),
            meta_info: meta_info.to_owned(),
            sender: Some(one_tx),
            _id: None,
        }))).unwrap_or(());

        let run_id = one_rx.recv().unwrap_or(0);
        drop(one_rx);
        println!("run_id: {}", run_id);

        let density = config.N as f64 / f64::from(config.L * config.L);
        vicsek_info.write().unwrap().set_density(density);
        vicsek_info.write().unwrap().max_periodic = Some(ex_options.metric_options.max_periodic);
        vicsek_info.write().unwrap().displacement_distance = Some(ex_options.metric_options.displacement_distance);
        if ex_options.metric_options.spacial_entropies {
            vicsek_info.write().unwrap().set_cell_size(ex_options.metric_options.cell_size);
            let amount_cells = intervals(config.L, ex_options.metric_options.cell_size).unwrap().len().pow(2);
            let h_max = (1f64 / amount_cells as f64).log2();
            vicsek_info.write().unwrap().set_spacial_entropies_max(-h_max);
        }

        let species_colors = config.groups.keys().fold(FxHashMap::default(), |mut acc, s| {
            let species = i8::from_str(s).unwrap();
            acc.entry(s.to_string()).or_insert(format!("{:?}", species_to_color(species)));
            acc
        });

        vicsek_info.write().unwrap().set_species_colors(species_colors);

        let mut old_obs = vicsek.get_obs();

        let vis_settings = Arc::new(VisParams {
            image_size: ex_options.vis.image_size,
            vector_length: ex_options.vis.vector_length,
            marker_size: ex_options.vis.marker_size
        });

        let render_params = Arc::new(RenderParams {
            length: config.L,
            tmax: config.tmax,
        });

        let cell_size = ex_options.metric_options.cell_size;
        let order_parameter = ex_options.metric_options.order_parameter;
        let frequencies = ex_options.metric_options.frequencies;
        let speed_of_rotation = ex_options.metric_options.speed_of_rotation;
        let spacial_entropies = ex_options.metric_options.spacial_entropies;
        let number_species_changes = ex_options.metric_options.number_of_species_changes;
        let species_orientation = ex_options.metric_options.species_orientation;
        let number_of_rotations = ex_options.metric_options.number_of_rotations;
        let max_periodic = ex_options.metric_options.max_periodic;
        let dd = ex_options.metric_options.displacement_distance;
        let x_entropies = ex_options.metric_options.x_entropies;
        let y_entropies = ex_options.metric_options.y_entropies;

        let (tx, rx) = crossbeam::channel::bounded::<(Observation, MetricsParameters, Arc<VisParams>, Arc<RenderParams>)>(1000000);

        let bc_sender = sender.clone();

        let ex_name = ex_options.experiment_name.to_owned();
        let base_dir = ex_options.base_dir.as_ref().unwrap().to_owned();
        let save_svgs = ex_options.save_svgs;
        let save_cps = ex_options.checkpoints;
        
        let groups: Arc<FxHashSet<i8>> = Arc::new(config.groups.keys().map(|k| {
           i8::from_str(k).unwrap()
        }).collect());

        let mut start_positions = old_obs.positions.to_owned();
        let no_rotations = ex_options.metric_options.number_of_rotations;

        let handle = thread::spawn(move || {
            let path = Arc::new(base_dir.join(ex_name).join(format!("run_{}", run_id)));
            let vis_dir = Arc::new(path.join("vis"));
            let cp_dir = Arc::new(path.join("checkpoints"));
            if save_svgs || save_cps {
                create_dir_all(&*path).unwrap();
                vicsek_info.write().unwrap().set_svg_dir(vis_dir.to_str().unwrap().to_string());
            }
            let mut thread_joins = Vec::new();

            let mut delta_thetas = Array1::zeros(config.N);
            let mut periodicies = Array1::zeros(config.N);
            let mut rotating_agents = FxHashMap::<(usize, i8, bool), usize>::default();

            while let Ok((obs1, params, vis_settings, render_params)) = rx.recv() {
                let step = obs1.step;
                let prefix = (render_params.tmax.ilog10() + 1) as usize;

                let extinct_species = obs1.extinct_species(&groups);

                for s in extinct_species {
                    vicsek_info.write().unwrap().extinct_species.entry(s.to_string()).or_insert(obs1.step);
                }

                if obs1.step >= config.tskip {
                    if hb_sender.is_full() {
                        println!("The heartbeat channel is full. Consider raising it's capacity or increase the heartbeat frequency.")
                    }

                    if params.number_of_rotating_particles {
                        if let Some(other_obs) = &params.other_obs {

                            Zip::from(&mut delta_thetas)
                                .and(&mut periodicies)
                                .and(start_positions.rows_mut())
                                .and(obs1.positions.rows())
                                .and(&obs1.spins)
                                .map_collect(|delta_theta, p, mut sp, pos, spin| {
                                    let d = euclidean_distance(&sp.view(), &pos, config.L as f64 / 2f64).sqrt();
                                    if *p > params.max_periodic {
                                        for step in (obs1.step - *p)..obs1.step {
                                            rotating_agents.entry((step, *spin, true)).or_default();
                                            rotating_agents.entry((step, *spin, false)).or_default();
                                        }
                                        *p = 0;
                                        *delta_theta = 0.;
                                        sp[0] = pos[0];
                                        sp[1] = pos[1];
                                    } else if d <= params.r_rotation {

                                        if *delta_theta > 2. * PI {
                                            for step in (obs1.step - *p)..obs1.step {
                                                *rotating_agents.entry((step, *spin, false)).or_default() += 1;
                                            }
                                            *delta_theta = 0.;
                                            sp[0] = pos[0];
                                            sp[1] = pos[1];
                                            *p = 0;
                                        } else if *delta_theta < -2. * PI {
                                            for step in (obs1.step - *p)..obs1.step {
                                                *rotating_agents.entry((step, *spin, true)).or_default() += 1;
                                            }
                                            *delta_theta = 0.;
                                            sp[0] = pos[0];
                                            sp[1] = pos[1];
                                            *p = 0;
                                        }

                                    }
                            });
                            delta_thetas += &other_obs.diff_thetas(&obs1);
                            periodicies += 1;
                        }

                    }

                    hb_sender.send(obs1.collect_metrics(params)).unwrap();

                    if save_cps {

                        let filename = format!("checkpoint{step:0>prefix$}.json");
                        let cp_dir_clone = cp_dir.clone();
                        let checkpoint = obs1.clone();
                        thread::spawn(move || {
                            if !cp_dir_clone.exists() {
                                create_dir(&*cp_dir_clone).unwrap();
                            }
                            let cp_path = cp_dir_clone.join(filename);
                            let mut f = File::create(cp_path).unwrap();
                            f.write_all(&serde_json::to_vec_pretty(&checkpoint).unwrap())
                        });
                    }

                    if save_svgs {
                        let svg = obs1.render(&vis_settings, &render_params).unwrap();
                        let t_vis_dir = vis_dir.clone();
                        let filename = format!("pic{step:0>prefix$}.svg");
                        thread_joins.push(thread::spawn(move || {
                            if !t_vis_dir.exists() { create_dir(&*t_vis_dir).unwrap(); }

                            let svg_path = t_vis_dir.join(filename);
                            let mut f = File::create(svg_path).unwrap();
                            f.write_all(&svg.into_bytes()).unwrap();
                        }));
                    }
                }

                if (obs1.step % 250 == 0 && step >= config.tskip) || obs1.step == config.tmax - 1 {
                    let svg = obs1.render(&vis_settings, &render_params).unwrap();
                    let event = Arc::new(ObserverEvents::ArtifactAdded(ArtifactAdded {
                        name: format!("pic{step:0>prefix$}.svg"),
                        data: svg.into_bytes(),
                        metadata: (),
                        content_type: "svg".to_string(),
                    }));
                    bc_sender.send(event).unwrap_or(());

                    let event = Arc::new(ObserverEvents::ArtifactAdded(ArtifactAdded {
                        name: format!("checkpoint_{step:0>prefix$}.json"),
                        data: serde_json::to_vec_pretty(&obs1).unwrap(),
                        metadata: (),
                        content_type: "json".to_string(),
                    }));
                    bc_sender.send(event).unwrap_or(());
                }
            }
            if no_rotations {
                let rotation_order_parameter = rotating_agents.iter().map(|((step, spin, clockwise_rotation), count)| {
                    let direction = if *clockwise_rotation { "cw" } else { "ccw" };
                    Metric {
                        name: format!("number_of_{}_rotating_particles_{}", direction, *spin),
                        step: *step,
                        value: MetricValues::Usize(*count),
                        timestamp: Utc::now(),
                    }
                })
                    .sorted_by(|a, b| a.step.cmp(&b.step))
                    .collect();
                hb_sender.send(rotation_order_parameter).unwrap();
            }
        });

        for step in 0..config.tmax {
            let result = vicsek.step();
            match result {
                Ok(curr_obs) => {
                    let obs = curr_obs.clone(); // t + 1
                    let obs1 = mem::replace(&mut old_obs, curr_obs); // t
                    let vis_settings = vis_settings.clone();
                    let render_params = render_params.clone();

                    let params = MetricsParameters {
                        order_parameter,
                        frequencies,
                        speed_of_rotation,
                        spacial_entropies,
                        species_orientation,
                        number_of_species_changes: number_species_changes,
                        number_of_rotating_particles: number_of_rotations,
                        r_rotation: dd,
                        max_periodic,
                        length: config.L,
                        cell_size,
                        other_spins: Some(obs.spins.to_owned()),
                        other_obs: Some(obs),
                        v_0: config.v,
                        x_entropies,
                        y_entropies,
                    };

                    tx.send((obs1, params, vis_settings, render_params)).unwrap();
                }
                Err(error) => {
                    let event = Arc::new(Event::Failed(Failed {
                        fail_time: Utc::now(),
                        fail_trace: error.to_string(),
                    }));
                    sender.send(event).unwrap();
                    panic!("simulation failed!")
                }
            }
        }
        println!("simulation done");
        drop(tx);

        handle.join().unwrap();

        Self {
        }

    }
}

pub struct VicsekSimMgr<'a> {
    vicsek: Vicsek<'a>,
    tmax: usize,
    writer: Arc<Mutex<&'a mut dyn ConsumerHandler<Observation>>>,
    settings: &'a Settings,
    run_obj: Run,
}


impl<'a> VicsekSimMgr<'a> {
    pub fn new(settings: &'a Settings, writer: &'a mut dyn ConsumerHandler<Observation>) -> Self {
        Self {
            vicsek: Vicsek::new(&settings.vicsek),
            tmax: settings.vicsek.tmax,
            writer: Arc::new(Mutex::new(writer)),
            settings,
            run_obj: {
                let binding = Path::new(&settings.output_dir).join(&settings.run_file_name);
                let run_info_path = binding.to_str().unwrap();
                Run::new(run_info_path)
            }
        }
    }
}

impl SimMgr<Observation, Vicsek<'_>, Run> for VicsekSimMgr<'_> {

    fn simulate(&mut self) -> Result<Run, Box<dyn Error>> {

        self.run_obj.sync();
        let rb = HeapRb::<Observation>::new(CONSUMER_BUFFER_SIZE);
        let (mut prod, mut cons) = rb.split();
        self.run_obj.run_and_sync();

        let scope: Result<Run, Box<dyn Error>>= thread::scope(move |s| {
            let local_writer = self.writer.clone();

            // spawn thread to write results into a specified output file
            let handle: ScopedJoinHandle<'_, Result<(), Box<dyn Error + Send + Sync>>> = s.spawn(move || {local_writer.lock().as_mut().unwrap().handle(&mut cons)?; Ok(())});

            if self.vicsek.t >= self.settings.vicsek.tskip { prod.push(self.vicsek.get_obs()).unwrap() };

            // advance the model until tmax steps and collect the state of the model after each step and
            // safe it into a shared buffer
            let mut buffer = Vec::new();
            for _ in 0..self.tmax {

                let obs = match self.vicsek.step() {
                    Ok(obs) => { obs }
                    Err(error) => {
                        self.run_obj.fail_and_sync(&error.to_string());
                        panic!();
                    }
                };

                if obs.step >= self.settings.vicsek.tskip {buffer.push(obs)};
                if buffer.len() > PRODUCER_BUFFER_SIZE {
                    prod.push_iter(&mut buffer.clone().into_iter());
                    buffer.clear();
                }
            }

            prod.push_iter(&mut buffer.clone().into_iter());
            buffer.clear();

            handle.join().unwrap().unwrap();
            Ok::<Run, Box<dyn Error>>(self.run_obj.clone())
        });

        let mut run_obj = scope?;
        run_obj.complete_and_sync();

        Ok(run_obj)
    }
}

struct IndexedPosition<'a>(ArrayView1<'a, f64>, usize, f64);

impl Proximity for IndexedPosition<'_> {

fn distance(&self, other: &IndexedPosition<'_>) -> Self::Distance {
    euclidean_distance_rt(&self.0, &other.0, self.2)
}
type Distance = f64;
}

struct ProximityArrayView1<'a>(ArrayView1<'a, f64>, f64);

impl Proximity<IndexedPosition<'_>> for ProximityArrayView1<'_> {

fn distance(&self, other: &IndexedPosition<'_>) -> Self::Distance {
    euclidean_distance_rt(&self.0, &other.0, self.1)
}
type Distance = f64;
}

#[derive(Clone)]
pub(crate) struct Vicsek<'a> {
    settings: &'a settings::Vicsek,
    pub(crate) positions: Array2<f64>,
    pub(crate) thetas: Array1<f64>,
    pub(crate) spins: Array1<i8>,
    pub(crate) t: usize,
    lxy: f64,
    view_angle: Option<f64>,
    r_sqrd: f64,
    index: Vec<usize>,
    groups: FxHashMap<i8, (usize, Option<UpdateStrategy>)>,
    spin_alignment_strategies: FxHashMap<(i8, i8), Option<GroupAlignment>>
}

impl<'a> Vicsek<'a> {
    pub fn new(settings: &'a settings::Vicsek) -> Self {
        let number_of_boxes = if settings.L as f64 % settings.r != 0. { panic!("The program divides the domain into equal sized boxes for performance reasons. \
                Therefore L needs to be dividable by r which is not the case with the current settings. \
                Try to rescale all parameters.") } else { (settings.L as f64 / settings.r) as u16 };
        let indices = Vec::from_iter(0..settings.N);
        Self {
            settings,
            positions: Array2::random((settings.N, 2), Uniform::new(-0.5f64, 0.5f64)) * settings.L as f64,
            thetas: Array1::random(settings.N, Uniform::new(0., 2. * PI)),
            spins: {
                // The sum of the group amounts need to be N.
                let total_amount = settings.groups.iter().fold(0, |a, (_, (amount, _))| a + *amount);
                if total_amount != settings.N {
                    panic!("The sum of amounts needs to be settings.vicsek.N but it is {}", total_amount);
                }

                let group_portions = settings.groups.iter()
                    .fold(Array1::default(0), |spins, (spin, (amount, _))| concatenate![Axis(0), spins, Array1::from_elem(*amount, i8::from_str(spin).unwrap())]);
                group_portions
            },
            t: 0,
            lxy: settings.L as f64 / 2f64,
            view_angle: {
                let mut a = None;
                if let Some(angle) = settings.filter_bubble_angle {
                    a = Some((angle as f64).to_radians() / 2f64);
                }
                a
            },
            r_sqrd: settings.r * settings.r,
            index: indices,
            groups: FxHashMap::from_iter(settings.groups.clone().into_iter().map(|(species, (amount, strategy))| { (i8::from_str(&species).unwrap(), (amount, strategy)) })),
            spin_alignment_strategies: FxHashMap::from_iter(settings.group_alignments.clone().into_iter().map(|a| a.into())),
        }
    }
}

impl <'a>Sim<Observation> for Vicsek<'a> {
    fn step(&mut self) -> Result<Observation, Box<dyn Error>> {

        let vp_tree = VpTree::balanced(self.positions.rows().into_iter().zip(&self.index).map(|(p, i)| {
            IndexedPosition(p, *i, self.lxy)
        }));

        let mut candidates = Array1::from_elem(self.settings.N, FxHashSet::default());
        let mut threshold_conditions = Array1::from_elem(self.settings.N, false);

        par_azip!((i in &self.index, set in &mut candidates, threshold in &mut threshold_conditions, pos in self.positions.rows().into_producer()) {

            let theta_p = self.thetas[*i];
            let xy_i = &self.positions.slice(s![*i, ..]);

            let mut results = vp_tree.k_nearest_within(&ProximityArrayView1(pos, self.lxy), self.settings.N, self.settings.r);

            // calculate vector perpendicular to i's velocity vector
            let v_1 = -1f64;
            let v_2 = xy_i[0] / xy_i[1];

            results.retain(|candidate| {
                let neighbor = candidate.item;
                let xy_candidate = &self.positions.slice(s![neighbor.1, ..]);
                let theta_candidate =  self.thetas[neighbor.1];

                    (!self.settings.view_ahead_only | (sgn_det(xy_i, xy_candidate, &array![xy_i[0] + v_1, xy_i[1] + v_2].view(), self.lxy) >= 0f64)) & // view ahead condition
                    self.view_angle.map_or(true, |angle| { ((theta_p - angle) <= theta_candidate) & (theta_candidate <= (theta_p + angle)) }) // filter bubble condition
            });

            if set.len() < self.settings.threshold {
                set.retain(|candidate| candidate == i);
            }
            *threshold = set.len() >= self.settings.threshold;

            *set = &*set | &FxHashSet::from_iter(results.iter().map(|neighbor| { neighbor.item.1 }));
        });

        let mut sum_of_sines = Array1::<f64>::zeros(self.settings.N);
        let mut sum_of_cosines = Array1::<f64>::zeros(self.settings.N);

        let mut spin_updates = self.spins.clone();

        // calculate average heading among all particles in the neighborhood and update spins
        par_azip!((set in &candidates, sine in &mut sum_of_sines, cosine in &mut sum_of_cosines, spin in &self.spins, update in &mut spin_updates, i in &self.index) {
            let c = set - &FxHashSet::from_iter([*i]);
            (*cosine, *sine) = c.iter()
            .fold((self.thetas[*i].cos(), self.thetas[*i].sin()), |(cosine, sine), p| {
                let alignment = self.spin_alignment_strategies.get(&(*spin, self.spins[*p])).unwrap_or(&None).as_ref();

                alignment.map_or((cosine, sine), |alignment| {
                    // Alignment is active between the individual and its neighbor => apply alignment and rotation
                    let (align, rotate): (f64, f64) = alignment.into();
                    let value = self.thetas[*p] + rotate;
                    (cosine + align * value.cos(), sine + align * value.sin())
                })

            });
            // .reduce(|| (0., 0.), |(cosine, sine), (cosine1, sine1)| {(cosine + cosine1, sine + sine1)});

            let spins_of_candidates: Vec<_> = set.iter().map(|candidate| self.spins[*candidate]).collect();
            *update = self.groups.get(spin).as_ref().unwrap().1.as_ref().map_or(*spin, |strategy| strategy.spin_update(&spins_of_candidates, *spin, self.settings));
        });

        // update heading of all particles
        let mut noise = Array1::<f64>::zeros(self.thetas.len());
        let etas = Array1::random(self.thetas.len(), Uniform::new(-self.settings.noise_range, self.settings.noise_range + f64::MIN_POSITIVE)) * self.settings.eta;
        let etas2 = Array1::random(self.thetas.len(), Uniform::new(-self.settings.noise_range, self.settings.noise_range + f64::MIN_POSITIVE)) * self.settings.eta2;
        par_azip!((n in &mut noise, eta in &etas, eta2 in &etas2, adj in &threshold_conditions) *n = match adj { true => *eta, false => *eta2});
        self.thetas = Zip::from(&sum_of_sines).and(&sum_of_cosines).par_map_collect(|sine, cosine| sine.atan2(*cosine));
        self.thetas += &noise;

        // update spins
        self.spins = spin_updates;

        // update the positions of the particles
        self.positions += &(stack![Axis(1), self.thetas.map(|dx| dx.cos()), self.thetas.map(|dy| dy.sin())] * self.settings.v);

        // consider periodic boundaries
        self.positions -= &(2f64 * ((&self.positions / self.lxy).mapv(|v| v.trunc()) * self.lxy));
        self.t += 1;

        Ok(Observation {
            positions: self.positions.clone(),
            thetas: self.thetas.clone(),
            spins: self.spins.clone(),
            step: self.t
        })
    }

    fn render(&self) {
        todo!()
    }

    fn reset(&mut self) {
        todo!()
    }

    fn get_obs(&self) -> Observation {
        Observation {
            positions: self.positions.clone(),
            thetas: self.thetas.clone(),
            spins: self.spins.clone(),
            step: self.t
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Observation {
    pub positions: Array2<f64>,
    pub thetas: Array1<f64>,
    pub spins: Array1<i8>,
    pub step: usize,

}

impl Hash for Observation {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.step.hash(state);
    }
}

impl Eq for Observation {}

impl PartialEq for Observation {
    fn eq(&self, other: &Self) -> bool {
        self.step == other.step
    }
}

fn update_spin_variable_majority_with_threshold(spins_of_candidates: &[i8], spin: i8, settings: &settings::Vicsek, threshold: f64, ignored_spins: &Option<FxHashSet<i8>>) -> i8 {
    let relations = spins_of_candidates.iter().cloned()
        .filter(|s| ignored_spins.as_ref().map_or(true, |ignored_spins| !ignored_spins.contains(s)))
        .fold(FxHashMap::<i8, u8>::default(), |mut m, v| {
            *m.entry(v).or_default() += 1;
            m
         })
        .into_iter()
        .map(|(k, v)| (k, v as f64 / spins_of_candidates.len() as f64));

    relations
        .filter(|(_, v)| *v >= threshold)
        .max_set_by(|(_, ratio), (_, ratio1)| ratio.total_cmp(ratio1)) // group change towards the majority
        .choose(&mut thread_rng())
        .and_then(|(k, v)| (*k != spin).then_some((*k, *v))) // ensure random change if the majority is the own spin
        .unwrap_or_else(|| {
            // No group has crossed threshold to majority
            //        => random group change:
            //                  - no change with possibility 1 - eta_spin
            //                  - change with uniform selection probability over the remaining groups (groups.len() - 1)
            let weight = settings.eta_spin / (settings.groups.len() - 1).max(1) as f64;
            let s: Vec<(i8, f64)> = settings.groups.keys().map(|s| if i8::from_str(s).unwrap() == spin { (i8::from_str(s).unwrap(), 1. - settings.eta_spin) } else { (i8::from_str(s).unwrap(), weight) }).collect();
            let new_spin = s.choose_weighted(&mut rand::thread_rng(), |item| item.1).unwrap().0;
            (new_spin, 0.)
        }).0
}

fn update_spin_variable_majority(spins_of_candidates: &[i8], spin: i8, settings: &settings::Vicsek, ignored_species: &Option<FxHashSet<i8>>) -> i8 {
    let frequencies = spins_of_candidates.iter().cloned()
        .filter(|s| ignored_species.as_ref().map_or(true, |ignored_spins| !ignored_spins.contains(s)))
        .fold(FxHashMap::<i8, usize>::default(), |mut m, v| {
            *m.entry(v).or_default() += 1;
            m
        })
        .into_iter();

    frequencies
        .max_set_by_key(|(_, amount)| *amount) // group change towards the majority
        .choose(&mut thread_rng())
        .and_then(|(k, v)| (*k != spin).then_some((*k, *v))) // ensure random change if the majority is the own spin
        .unwrap_or_else(|| {
            // No group has crossed threshold to majority
            //        => random group change:
            //                  - no change with possibility 1 - eta_spin
            //                  - change with uniform selection probability over the remaining groups (groups.len() - 1)
            let weight = settings.eta_spin / (settings.groups.len() - 1).max(1) as f64;
            let s: Vec<(i8, f64)> = settings.groups.keys().map(|s| if i8::from_str(s).unwrap() == spin { (i8::from_str(s).unwrap(), 1. - settings.eta_spin) } else { (i8::from_str(s).unwrap(), weight) }).collect();
            let new_spin = s.choose_weighted(&mut rand::thread_rng(), |item| item.1).unwrap().0;
            (new_spin, 0)
        }).0
}

fn update_spin_majority_alt(spins_of_candidates: &[i8], spin: i8, settings: &settings::Vicsek) -> i8 {
    let frequencies = FxHashMap::from_iter(spins_of_candidates.iter().cloned().counts());

    frequencies
        .into_iter()
        .max_set_by_key(|(_, amount)| *amount)
        .iter()
        .filter(|(s, _)| *s != spin)
        .choose(&mut thread_rng())
        .and_then(|(s, _)| {
            let weighted_spins = vec![(*s, settings.eta_spin), (spin, 1f64 - settings.eta_spin)];
            let new_spin = weighted_spins.choose_weighted(&mut thread_rng(), |(_, weight)| *weight).unwrap().0;
            Some((new_spin, 0))
        })
        .unwrap_or((spin, 0))
        .0
}

fn update_spin_minority(spins_of_candidates: &[i8], spin: i8, to_spin: i8, settings: &settings::Vicsek) -> i8 {
    let spin_set = FxHashSet::from_iter(spins_of_candidates);
    if spin_set.len() == 1 && spins_of_candidates.len() > 1 {
        return to_spin
    }
    spin
}

pub trait MetricsCollectable {
    type Params;

    fn collect_metrics(&self, params: Self::Params) -> Vec<Metric>;
}

impl Observation {
    pub fn position_vector(&self) -> Array2<f64> {
        stack![Axis(1), self.thetas.map(|x| (*x).cos()), self.thetas.map(|y| (*y).sin())]
    }

    pub fn velocities(&self, v_0: f64) -> Array2<f64> {
        let position_vector = self.position_vector();
        v_0 * &position_vector
    }

    pub fn velocities_per_species(&self, v_0: f64) -> FxHashMap<i8, Array2<f64>> {
        let velocities = self.velocities(v_0);
        let map = zip(&self.spins, velocities.rows())
            .into_grouping_map()
            .fold(Array2::<f64>::default((0,2)), |mut acc, _, v| {
                acc.push_row(v).unwrap();
                acc
            });
        let mut fx_map = FxHashMap::default();
        fx_map.extend(map.into_iter().map(|(k, v)| (*k, v)));
        fx_map
    }

    pub fn order_parameter(&self, v_0: f64) -> FxHashMap<i8, f64> {
        let velocities = self.velocities_per_species(v_0);

        velocities.into_iter().fold(FxHashMap::<i8, f64>::default(), |mut m, (s, v)| {
            let count = v.len_of(Axis(0));
            let avg_velocity = v.sum_axis(Axis(0));
            let len_avg_velocity = avg_velocity.map(|xy| xy.powi(2)).sum().sqrt();
            let order_parameter = len_avg_velocity / (count as f64 * v_0);
            m.entry(s).or_insert(order_parameter);
            m
        })
    }

    pub fn species_frequencies(&self) -> FxHashMap<i8, usize> {
        let counts = self.spins.iter().counts();
        let mut fx_map = FxHashMap::default();
        fx_map.extend(counts.into_iter().map(|(k, v)| (*k, v)));
        fx_map
    }

    pub fn number_of_species_changes(&self, other: &ArrayView1<i8>) -> u64 {
        Zip::from(&self.spins).and(other).fold(0u64, |mut acc, s1, s2| {
            acc += u64::from(s1 != s2);
            acc
        })
    }

    pub fn species_avg_orientation(&self, v_0: f64) -> ParameterPerSpecies<f64> {
        zip(&self.spins, &self.thetas).into_grouping_map().fold(Array1::default(2), |mut array, _, v| {
            array.scaled_add(v_0, &array![v.cos(), v.sin()]);
            array
        })
            .iter()
            .fold(ParameterPerSpecies(FxHashMap::default()), |mut m, (k, v)| {
                let cosine = v[0];
                let sine = v[1];
                let angle = sine.atan2(cosine);
                m.0.entry(**k).or_insert(angle);
                m
            })
    }

    pub fn abs_diff_theta_per_species(&self, other: &Observation, v_0: f64) -> ParameterPerSpecies<f64> {
        let mut avg = self.species_avg_orientation(v_0);
        let avg_other = other.species_avg_orientation(v_0);
        avg -= avg_other;

        let abs_diff_angles: FxHashMap<i8, f64> = avg.iter()
            .map(|(k, v)| {
                let diff = (*v).abs();
                let rotated_diff = 2f64 * PI - diff;

                (*k, diff.min(rotated_diff))
            })
            .collect();

        ParameterPerSpecies(abs_diff_angles)
    }

    pub fn diff_thetas(&self, other: &Observation) -> Array1<f64> {
        (&self.thetas - &other.thetas).map(|theta| {
            let mut a = *theta;
            if *theta > PI {
                a -= 2. * PI;
            } else if *theta < -PI {
                a += 2. * PI;
            }
            a
        })
    }

    fn distribute_particles_in_cells(&self, length: u16, cell_size: f64) -> Array2<usize> {
        let interval = intervals(length, cell_size).unwrap();
        let mut cells = Array2::default(self.positions.dim());
        par_azip!((mut cell in cells.rows_mut().into_producer(), position in self.positions.rows().into_producer()) {
            cell[0] = interval.binary_search_by(|row_range| cmp(position[0], row_range)).unwrap();
            cell[1] = interval.binary_search_by(|column_range| cmp(position[1], column_range)).unwrap();
        });
        cells
    }

    pub fn spacial_entropy(&self, length: u16, s: f64) -> ParameterPerSpecies<f64> {
        let cell_of_particles = self.distribute_particles_in_cells(length, s);
        let species_counts = self.species_frequencies();

        let particle_per_cell_counts = zip(self.spins.iter(), cell_of_particles.rows().into_iter()).counts();

        particle_per_cell_counts.iter().fold(FxHashMap::<_, f64>::default(), |mut m, ((species, _), count)| {
            let fraction = *count as f64 / *species_counts.get(*species).unwrap() as f64;

            *m.entry(**species).or_default() -= fraction * fraction.log2();

            m
        }).into()
    }

    pub fn axis_entropy(&self, length: u16, s: f64, axis: u8) -> ParameterPerSpecies<f64> {
        let cell_of_particles = self.distribute_particles_in_cells(length, s);
        let species_counts = self.species_frequencies();

        let particles_axis_count = zip(self.spins.iter(), cell_of_particles.column(axis.into())).counts();

        particles_axis_count.iter().fold(FxHashMap::<_, f64>::default(), |mut m, ((species, _), count)| {
            let fraction = *count as f64 / *species_counts.get(*species).unwrap() as f64;
            *m.entry(**species).or_default() -= fraction * fraction.log2();

            m
        }).into()
    }
    
    pub fn extinct_species(&self, species: &FxHashSet<i8>) -> FxHashSet<i8> {
        let active_species = self.spins.iter().unique().cloned().collect();
        species - &active_species
    }

    fn convert_parameter_per_species_to_metric<T>(&self, parameter: ParameterPerSpecies<T>, name: String) -> Vec<Metric>
        where MetricValues: From<T>
    {
        let mut metrics = Vec::default();
        for (species, value) in parameter.0.into_iter() {
            let value = MetricValues::from(value);
            let name = format!("{}_of_species_{}", name, species);
            let metric = Metric {
                name,
                step: self.step,
                value,
                timestamp: Utc::now(),
            };
            metrics.push(metric);
        }

        metrics
    }
}

fn distribute_particles_in_cells(positions: &Array2<f64>, length: u16, cell_size: f64) -> Array2<usize> {
    let interval = intervals(length, cell_size).unwrap();
    let mut cells = Array2::default(positions.dim());
    par_azip!((mut cell in cells.rows_mut().into_producer(), position in positions.rows().into_producer()) {
            cell[0] = interval.binary_search_by(|row_range| cmp(position[0], row_range)).unwrap();
            cell[1] = interval.binary_search_by(|column_range| cmp(position[1], column_range)).unwrap();
        });
    cells
}

impl MetricsCollectable for Observation {
    type Params = MetricsParameters;

    fn collect_metrics(&self, params: Self::Params) -> Vec<Metric> {
        let mut metrics = Vec::default();

        if params.speed_of_rotation {
            if let Some(other_obs) = params.other_obs {
                let speed_of_rotation = self.abs_diff_theta_per_species(&other_obs, params.v_0);
                let name = "speed_of_rotation".to_string();
                metrics.append(&mut self.convert_parameter_per_species_to_metric(speed_of_rotation, name));
            }
        }

        if params.spacial_entropies {
            let spatial_entropy = self.spacial_entropy(params.length, params.cell_size);
            let name = "spatial_entropy".to_string();
            metrics.append(&mut self.convert_parameter_per_species_to_metric(spatial_entropy, name));
        }

        if params.order_parameter {
            let order_parameter = self.order_parameter(params.v_0);
            let name = "order_parameter".to_string();
            metrics.append(&mut self.convert_parameter_per_species_to_metric(order_parameter.into(), name));
        }

        if params.frequencies {
            let species_frequencies = self.species_frequencies();
            let name = "frequency".to_string();
            metrics.append(&mut self.convert_parameter_per_species_to_metric(ParameterPerSpecies::<usize>::from(species_frequencies), name));
        }

        if params.species_orientation {
            let mut species_heading = self.species_avg_orientation(params.v_0);
            species_heading.0.values_mut().for_each(|v| { *v = v.to_degrees() });
            let name = "species_orientation".to_string();
            metrics.append(&mut self.convert_parameter_per_species_to_metric(species_heading, name));
        }

        if params.number_of_species_changes {
            if let Some(spins) = params.other_spins {
                let species_changes = self.number_of_species_changes(&spins.view());
                let name = "species_changes".to_string();
                metrics.push(Metric {
                    name,
                    step: self.step,
                    value: MetricValues::UInt(species_changes),
                    timestamp: Utc::now(),
                });
            }

        }

        if params.x_entropies {
            let x_entropies = self.axis_entropy(params.length, params.cell_size, 0);
            let name = "x_entropy".to_string();
            metrics.append(&mut self.convert_parameter_per_species_to_metric(x_entropies, name));
        }

        if params.y_entropies {
            let y_entropies = self.axis_entropy(params.length, params.cell_size, 1);
            let name = "y_entropy".to_string();
            metrics.append(&mut self.convert_parameter_per_species_to_metric(y_entropies, name));
        }

        metrics
    }
}

fn convert_parameter_per_species_to_metric<T>(parameter: ParameterPerSpecies<T>, name: String, step: usize, timestamp: DateTime<Utc>) -> Vec<Metric>
where MetricValues: From<T>
{
    let mut metrics = Vec::default();
    for (species, value) in parameter.0.into_iter() {
        let value = MetricValues::from(value);
        let name = format!("{}_of_species_{}", name, species);
        let metric = Metric {
            name,
            step,
            value,
            timestamp,
        };
        metrics.push(metric);
    }

    metrics
}

pub struct MetricsParameters {
    order_parameter: bool,
    frequencies: bool,
    speed_of_rotation: bool,
    spacial_entropies: bool,
    species_orientation: bool,
    number_of_species_changes: bool,
    number_of_rotating_particles: bool,
    r_rotation: f64,
    max_periodic: usize,
    length: u16,
    cell_size: f64,
    other_obs: Option<Observation>,
    other_spins: Option<Array1<i8>>,
    v_0: f64,
    x_entropies: bool,
    y_entropies: bool
}

fn intervals(l: u16, size: f64) -> Result<Vec<Range<f64>>, Box<dyn Error + Send + Sync>> {
    if l as f64 % size != 0. {return Err(Box::from("error"))}
    let mut intervals = Vec::new();
    let no_intervals = (l as f64 / size) as usize;
    for i in 0..no_intervals {
        intervals.push(((i as f64 * size) -l as f64 / 2.)..(((i + 1) as f64 * size) - l as f64 / 2. + 1.));
    }
    Ok(intervals)
}

#[derive(PartialEq, Eq, Clone, Default, Serialize, Debug)]
pub struct ParameterPerSpecies<T> (pub FxHashMap<i8, T>);

impl <T> ParameterPerSpecies<T> {
    delegate! {
        to self.0 {
            pub fn insert(&mut self, k: i8, v: T) -> Option<T>;
            pub fn entry(&mut self, key: i8) -> Entry<'_, i8, T>;
            pub fn get(&self, key: &i8) -> Option<&T>;
            pub fn get_mut(&mut self, key: &i8) -> Option<&mut T>;
            pub fn iter(&self) -> Iter<'_, i8, T>;
            pub fn iter_mut(&mut self) -> IterMut<'_, i8, T>;
            pub fn values_mut(&mut self) -> ValuesMut<'_, i8, T>;
        }
    }
}

impl<T> AddAssign for ParameterPerSpecies<T> where T: AddAssign + Sized + Default + Copy {
    fn add_assign(&mut self, rhs: Self) {
        rhs.0.iter().for_each(|(k, v)| {
            *self.0.entry(*k).or_default() += *v;
        });
    }
}

impl<T> AddAssign<&Self> for ParameterPerSpecies<T> where T: AddAssign + Sized + Default + Copy {
    fn add_assign(&mut self, rhs: &Self) {
        rhs.0.iter().for_each(|(k, v)| {
            *self.0.entry(*k).or_default() += *v;
        });
    }
}

impl<T> SubAssign for ParameterPerSpecies<T> where T: SubAssign + Sized + Default + Copy {
    fn sub_assign(&mut self, rhs: Self) {
        rhs.iter().for_each(|(k, v)| {
            *self.entry(*k).or_default() -= *v;
        });
    }
}

impl<T> SubAssign<&Self> for ParameterPerSpecies<T> where T: SubAssign + Sized + Default + Copy {
    fn sub_assign(&mut self, rhs: &Self) {
        rhs.iter().for_each(|(k, v)| {
            *self.entry(*k).or_default() -= *v;
        });
    }
}

fn smaller_map<T>(a: ParameterPerSpecies<T>, b: ParameterPerSpecies<T>) -> (ParameterPerSpecies<T>, ParameterPerSpecies<T>) {
    if a.0.len() < b.0.len() { (a, b) } else { (b, a) }
}

impl<T> Add for ParameterPerSpecies<T> where T: AddAssign + Sized + Default + Copy {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let (smaller, bigger) = smaller_map(self, rhs);
        smaller.0.iter().fold::<FxHashMap<i8, T>, _>(bigger.0, |mut m, (k,v)| {
            *m.entry(*k).or_default() += *v;
            m
        }).into()
    }
}

impl<T> Sub for ParameterPerSpecies<T> where T: SubAssign + Sized + Default + Copy {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        let (smaller, bigger) = smaller_map(self, rhs);
        smaller.0.iter().fold::<FxHashMap<i8, T>, _>(bigger.0, |mut m, (k,v)| {
            *m.entry(*k).or_default() -= *v;
            m
        }).into()
    }
}

impl<T> Add for &ParameterPerSpecies<T> where T: AddAssign + Sized + Copy + Default {
    type Output = ParameterPerSpecies<T>;

    fn add(self, rhs: Self) -> Self::Output {
        let (smaller, bigger) = if self.0.len() < rhs.0.len() { (&self.0, &rhs.0) } else { (&rhs.0, &self.0) };
        let init = bigger.clone();
        smaller.iter().fold::<FxHashMap<i8, T>, _>(init, |mut m, (k, v)| {
            *m.entry(*k).or_default() += *v;
            m
        }).into()
    }
}

impl <T>From<FxHashMap<i8, T>> for ParameterPerSpecies<T> where T: Sized + Copy + Default {
    fn from(value: FxHashMap<i8, T>) -> Self {
        ParameterPerSpecies(value)
    }
}

impl From<FxHashMap<i8, usize>> for ParameterPerSpecies<f64> {
    fn from(value: FxHashMap<i8, usize>) -> Self {
        let mut map = FxHashMap::default();
        map.extend(value.into_iter().map(|(k, v)| {
            (k, v as f64)
        }));
        ParameterPerSpecies(map)
    }
}

fn neighboring_boxes(i: u16, j: u16, l: u16) -> [(u16, u16); 9] {
    let rshift_i = (i + 1) % l;
    let lshift_i = if i == 0 { l - 1} else { i - 1 };
    let rshift_j = (j + 1) % l;
    let lshift_j = if j == 0 { l - 1} else { j - 1 };
    [(i, j), (rshift_i, j), (lshift_i, j), (rshift_i, rshift_j), (i, lshift_j), (lshift_i, lshift_j),
         (rshift_i, lshift_j), (lshift_i, rshift_j), (i, rshift_j)]
}

fn sgn_det(point1: &ArrayView1<f64>, point2: &ArrayView1<f64>, point3: &ArrayView1<f64>, lxy: f64) -> f64 {
    let vec1 = diff_pbc(point3, point1, lxy);
    let vec2 = diff_pbc(point2, point1, lxy);
    vec1[0] * vec2[1] - vec1[1] * vec2[0]
}

fn diff_pbc(point1: &ArrayView1<f64>, point2: &ArrayView1<f64>, lxy: f64) -> Array1<f64> {
    let mut diff: Array1<f64> = point1 - point2;
    diff -= &((&diff / (2f64 * lxy)).mapv(|dxy| dxy.round()) * 2f64 * lxy);
    diff
}

fn euclidean_distance(point1: &ArrayView1<f64>, point2: &ArrayView1<f64>, lxy: f64) -> f64 {
    let diff = diff_pbc(point1, point2, lxy);
    (&diff * &diff).sum()
}

fn euclidean_distance_rt(point1: &ArrayView1<f64>, point2: &ArrayView1<f64>, lxy: f64) -> f64 {
    let diff = diff_pbc(point1, point2, lxy);
    (&diff * &diff).sum().sqrt()
}

pub fn dist_to_mat(
    points: &Array2<f64>,
    dist_fn: fn(&ArrayView1<f64>, &ArrayView1<f64>, lxy: f64) -> f64,
    lxy: f64
) -> Array2<f64> {
    // initialize array
    let mut distances = Array::zeros((points.nrows(), points.nrows()));
    let idx = indices_of(&distances);
    // helper function to get the entries of points
    let func = |i: usize, j: usize| -> f64 {
        dist_fn(&points.slice(s![i, ..]), &points.slice(s![j, ..]), lxy)
    };
    // apply function to matrix with index array in lock-step
    par_azip!((c in &mut distances, (i,j) in idx){*c = func(i,j)});
    distances
}

fn cmp(a: f64, b: &Range<f64>) -> Ordering {
    if b.contains(&a) | (a == b.end) { Ordering::Equal }
    else if a < b.start { Ordering::Greater }
    else { Ordering::Less }
}

#[cfg(test)]
mod tests {
    use std::collections::{HashMap, HashSet};
    use fxhash::{FxHashMap, FxHashSet};
    use ndarray::parallel::prelude::{IntoParallelIterator, IntoParallelRefMutIterator, ParallelIterator};
    use crate::settings;
    use crate::settings::UpdateStrategy;
    use ndarray::{Array2, Zip, Array1, Axis, arr1, Array, par_azip, IntoNdProducer, concatenate};
    use crate::vicsek::{cmp, dist_to_mat, euclidean_distance, intervals, ParameterPerSpecies, sgn_det};
    use ndarray_rand::rand_distr::Uniform;
    use ndarray_rand::RandomExt;
    use std::f64::consts::PI;
    use std::ops::{Range};
    use std::sync::{Arc, Mutex};

    #[test]
    fn hashmap_tests() {
        let mut set1 = HashSet::new();
        let mut set2 = HashSet::new();

        set1.insert(5);
        set2.insert(4);
        set2.insert(10);
        set1 = &set1 | &set2;

        let range_xy = |i: f64| (i - 3.5.. i - 3.5 + 1.);
        let positions: Array2<f64> = Array2::random((3, 2), Uniform::new(-3.5, 3.5));

        let _particles_box = Array1::from_elem(3, (u8::MAX, u8::MAX));

        let mut particles_box_map = HashMap::new();
        for i in 0..3 as usize {
            particles_box_map.insert(i, (u8::MAX, u8::MAX));
        }
        let mut boxes = HashMap::new();
        for i in 0..7 {
            for j in 0..7 {
                boxes.insert((i as u8, j as u8), HashSet::<usize>::new());
            }
        }
        boxes.par_iter_mut().for_each(|((i, j), set)| {
            let mutex = Arc::new(Mutex::new(set));
            let c_mutex = Arc::clone(&mutex);
            Zip::indexed(positions.rows().into_producer()).par_for_each(move |index, position| {
                if range_xy(*i as f64).contains(&position[0]) & range_xy(*j as f64).contains(&position[1]) {
                    println!("Particle {} in box {:?}", index, (i,j));
                    c_mutex.lock().unwrap().insert(index);
                }
            });
        });

        println!("{:?}", boxes);

    }

    #[test]
    fn test_add_parameter_per_species() {
        let mut p = ParameterPerSpecies::default();
        let mut p1 = ParameterPerSpecies::default();
        p.insert(0i8, 5usize);
        p.insert(1, 3);

        p1.insert(0i8, 3usize);
        p1.insert(2, 1);

        p += p1;

        assert_eq!(*p.get(&0).unwrap(), 8usize);
        assert_eq!(*p.get(&1).unwrap(), 3usize);
        assert_eq!(*p.get(&2).unwrap(), 1usize);
    }

    #[test]
    fn dist_matrix_test() {
        let mut intervals: Vec<Range<f64>> = Vec::new();
        for i in 0..7 {
            intervals.push((i as f64 - 3.5)..(i as f64 - 2.5));
        }
        println!("{:?}", intervals);

        assert_eq!(intervals.binary_search_by(|range| cmp(-2.5, range)), Ok(1));

        println!("{:?}", cmp(0.5, &(0.0..3.5)));

        let point1 = arr1(&[-3.5f64, 0f64]);
        let point2 = arr1(&[3f64, 0f64]);
        let lxy = 3.5f64;
        assert_eq!(euclidean_distance(&point1.view(), &point2.view(), lxy), 0.5f64);
    }

    #[test]
    fn test_broadcast() {
        let view_angle = 30f64 * (PI / 180f64);
        let threshold = 2;
        let positions: Array2<f64> = Array2::random((3, 2), Uniform::new(-3.5, 3.5));
        let mut thetas: Array1<f64> = Array1::random(3, Uniform::new(0., 2. * PI));
        let distances = dist_to_mat(&positions, euclidean_distance, 3.5);
        let adjacency_matrix = distances.mapv(|dist| dist < 1.);
        let mut adjacency_matrix2 = Array::from_elem((positions.nrows(), positions.nrows()), false);
        let neighbor_counts = adjacency_matrix.fold_axis(Axis(1), 0, |count, is_neighbor| *count + *is_neighbor as i32);

        par_azip!((index (i,j), dist in &distances, adj in &mut adjacency_matrix2) {
                *adj = (*dist < 1f64) & // check interaction radius
                ((thetas[i] - view_angle) <= thetas[j]) & (thetas[j] <= thetas[i] + view_angle) & // check view angle
                ((i == j) | (neighbor_counts[i] >= threshold)) // check number of neighbors
            });

        println!("neighbor_count: {:?}", neighbor_counts);
        println!("adjacency matrix: {:?}", adjacency_matrix2);
        let headings_matrix = Zip::from(&adjacency_matrix).and_broadcast(&thetas).par_map_collect(|neighbor, heading| match neighbor { false => None, true => Some(*heading)});
        let sum_of_sines = headings_matrix.map(|heading| heading.unwrap_or(0.).sin()).sum_axis(Axis(1));
        let sum_of_cosines = headings_matrix.map(|heading| heading.unwrap_or(0.).cos()).sum_axis(Axis(1));
        thetas = Zip::from(&sum_of_sines).and(&sum_of_cosines).par_map_collect(|sine, cosine| sine.atan2(*cosine));

        println!("theta: {:?}", &thetas);
        headings_matrix.map(|item| item.unwrap_or(0.).sin());
    }

    #[test]
    fn test_something() {
        let groups = vec![(-1, 0.2), (1, 0.4), (0, 0.4)];
        let N = 1250;
        let mut spins = groups.iter()
            .map(|(spin, portion)| (*spin, (*portion * N as f64).floor() as usize, (*portion * N as f64).fract())).collect::<Vec<(i32, usize, f64)>>();
        spins.sort_by(|(_, _, fract), (_, _, fract1)| fract.partial_cmp(fract1).unwrap());
        spins.reverse();

        let mut rest = spins.iter().fold(0., |sum, (_, _, fract)| sum + *fract) as usize;

        let spins = spins.iter().map(|(spin, amount, _)| {
            if rest > 0 {
                rest -= 1;
                return (*spin, *amount + 1)
            }
            (*spin, *amount)
        }).collect::<Vec<(i32, usize)>>();

        println!("{:?}", spins);

        let spins = spins.iter().cloned().fold(Array1::default(0), |spins, (spin, amount)| concatenate![Axis(0), spins, Array1::from_elem(amount, spin)]);

        assert_eq!(spins.shape()[0], N);
    }

    #[test]
    fn test_det() {
        let point1 = arr1(&vec![8.04674, -5.02307f64]);
        let point2 = arr1(&vec![-7.36175f64, -6.47352f64]);
        //let theta = -135f64 * PI / 180f64;
        let theta = 3.437494;
        let point3 = arr1(&vec![point1[0] + (theta - PI / 2f64).cos(), point1[1] + (theta - PI / 2f64).sin()]);
        let sgn = sgn_det(&point1.view(), &point2.view(), &point3.view(), 15.0);
        println!("det: {}", sgn);
        assert_eq!(sgn >= 0f64, true);
    }

    #[test]
    fn test_spin_update() {
        let spin = -1i8;
        let spin1 = 1i8;
        let set = vec![spin, -1, 1, -1, -1];
        let set1 = vec![spin, 1, 1, 1, -1, -1, 1];
        let set2 = vec![spin, 1];
        let new_spin: i8 = set.iter().sum();
        assert!(!(new_spin * spin).is_negative());
        let new_spin: i8 = set1.iter().sum();
        assert!((new_spin * spin).is_negative());
        let new_spin: i8 = set2.iter().sum();
        assert!(!(new_spin * spin).is_negative());

        let max = set.into_par_iter().fold(|| FxHashMap::<i8, u8>::default(), |mut m, v| {
                *m.entry(v).or_default() += 1;
                m
            }).reduce(|| FxHashMap::<i8, u8>::default(), |mut m, n| { m.iter_mut().for_each(|(k, v)| *v += n.get(k).unwrap()); m });

        println!("{:?}", max);

        let set = [spin1, -1, -1, -1, 1, -1, -1, -1, 1, -1];

        let mut settings: settings::Vicsek = Default::default();
        settings.eta_spin = 0.;
        let new_spin = UpdateStrategy::MajorityWithThreshold(0.7, Some(FxHashSet::from_iter([-1i8]))).spin_update(&set.to_vec(), spin1, &settings);
        assert_eq!(new_spin, 1);
        settings.eta_spin = 1.;
        assert_eq!(UpdateStrategy::MajorityWithThreshold(0.7, None).spin_update(&set.to_vec(), -1, &settings), 1);
        assert_eq!(UpdateStrategy::MajorityWithThreshold(0.7, None).spin_update(&vec![], -1, &settings), 1);

        let mut map: FxHashMap<i8, u8> = FxHashMap::default();
        map.insert(-1, 7);
        let mut map1 = FxHashMap::default();
        map1.insert(1i8, 4u8);

        println!("{:?}", map.iter().chain(map1.iter()).collect::<Vec<_>>().into_iter().fold(FxHashMap::<i8, u8>::default(), |mut m, (k, v)| {
            *m.entry(*k).or_default() += *v;
            m
        })
        );

    }

    #[test]
    fn test_range_compare() {
        let i = intervals(7, 1.).unwrap();
        let x = -1.6f64;
        let x1 = -3.5;
        let result = i.binary_search_by(|item| cmp(x, item));
        let r1 = i.binary_search_by(|item| cmp(x1, item));
        assert_eq!(result, Ok(1));
        assert_eq!(r1, Ok(0));
        intervals(7, 0.5).unwrap();
    }
}
