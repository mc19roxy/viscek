use std::fs::{File};
use std::io::{Write};
use std::num::ParseFloatError;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use clap::{Parser, Subcommand, Args};
use crate::io::{JsonVicsekWriter, ObsFileReader};
use crate::settings::{MultiVicsek, Settings, Visualisation};
use crate::simulation::SimMgr;
use crate::vicsek::{VicsekSimMgr};
use crate::vis::{Render, RenderParams, VisParams};
use rayon::iter::ParallelBridge;
use rayon::iter::IntoParallelIterator;
use rayon::iter::ParallelIterator;
use crate::run::{ExperimentOptions, FFmpegSettings, MetricsOptions, MongoObserverOptions, multi_experiment, ObserverOptions, process_video};
use url::Url;

#[derive(Parser)]
#[command(author, version, about, long_about=None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
    #[arg(short)]
    mongo: Option<Url>,
    #[command(flatten)]
    sacred_options: SacredOptions
}

#[derive(Subcommand)]
enum Commands {
    RunC {
        #[arg(short, long)]
        config: Option<PathBuf>,
    },
    Visualize(Visualisation),
    Run {
        #[arg(short, long)]
        config_path: PathBuf,
        #[arg(default_value_t = String::from(""), long)]
        experiment_name: String,
        #[arg(long)]
        base_dir: Option<PathBuf>,
        #[arg(long)]
        run_id: Option<i64>,
        #[arg(long)]
        save_svgs: bool,
        #[arg(long)]
        checkpoints: bool,

        // metric parameters
        #[command(flatten)]
        metrics: MetricArgs,
        #[arg(long, group = "a", conflicts_with = "metrics")]
        all_metrics: bool,
        #[arg(long, default_value_t = 1., requires = "a")]
        cell_size: f64,
        #[arg(long, default_value_t = 25, requires = "b")]
        max_periodic: usize,
        #[arg(long, short, default_value_t = 0.25, requires = "b")]
        displacement_distance: f64,

        // visualisation
        #[arg(long, default_value_t = 600)]
        image_size: u32,
        #[arg(long, default_value_t = 5)]
        particle_length: u32,
        #[arg(long, default_value_t = 1)]
        marker_size: u32,
    },
    CreateVideo {
        #[arg(long, short)]
        run_id: i64,
        #[command(flatten)]
        ffmpeg_settings: FFmpegSettings,
        #[arg(long)]
        image_name_pattern: Option<String>,
    }
}

#[derive(Args)]
#[group(multiple = true, id = "metrics")]
struct MetricArgs {
    #[arg(long)]
    order_parameter: bool,
    #[arg(long)]
    frequencies: bool,
    #[arg(long, group = "a")]
    spacial_entropies: bool,
    #[arg(long, group = "a")]
    x_entropies: bool,
    #[arg(long, group = "a")]
    y_entropies: bool,
    #[arg(long)]
    speed_of_rotation: bool,
    #[arg(long)]
    species_orientation: bool,
    #[arg(long)]
    number_of_species_changes: bool,
    #[arg(long, group = "b")]
    number_of_rotations: bool,
}

#[derive(Clone)]
struct FloatRange {
    start: f64,
    end: f64,
    step: f64
}

#[derive(Args)]
pub struct SacredOptions {
    #[arg(long, default_value_t = 10000)]
    pub(crate) hb_channel_capacity: usize,
    #[arg(long, default_value_t = 5)]
    pub(crate) hb_frequency: i64
}

impl FromStr for FloatRange {
    type Err = ParseFloatError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let values: Vec<_> = s.split(" ").collect();
        Ok(
            Self{
                start: f64::from_str(values[0])?,
                end: f64::from_str(values[1])?,
                step: f64::from_str(values[2])?,
            }
        )
    }
}

pub(crate) fn run() {
    let cli = Cli::parse();

    match cli.command.unwrap() {
        Commands::RunC {config} => {
            let settings = Settings::new2(config);
            let mut file = File::options().write(true).truncate(true).create(true).open(Path::new(&settings.output_dir).join(&settings.config_file_name)).unwrap();
            file.write_all(ron::ser::to_string_pretty(&settings, ron::ser::PrettyConfig::default()).unwrap().as_bytes()).unwrap();

            let mut binding = JsonVicsekWriter::new(&settings);
            let mut mgr = VicsekSimMgr::new(&settings, &mut binding);
            mgr.simulate().unwrap();
        }
        Commands::Visualize(vis) => {
            let settings = Settings::new(&PathBuf::from("config/default.ron"), &PathBuf::from(""), &PathBuf::from(&vis.input).join(&vis.config_file_name)).unwrap();
            let reader = ObsFileReader::new(Path::new(&vis.input).join(&vis.data_file_name));
            let render_params = RenderParams {
                length: settings.vicsek.L,
                tmax: settings.vicsek.tmax,
            };
            reader.par_bridge().into_par_iter().for_each(|obs| {
                let _: () = obs.render(&vis, &render_params).unwrap();
            });
        }
        Commands::Run { config_path , experiment_name, base_dir, run_id, save_svgs, checkpoints, metrics, all_metrics, cell_size, max_periodic, displacement_distance, image_size, particle_length, marker_size } => {
            println!("start multiexperiment");
            let mut observer_settings= ObserverOptions::default();
            if let Some(mut url) = cli.mongo {
                let mongodb_name = parse_mongo_dbname(&url);
                println!("Found configuration for MongoObserver:");
                println!("\tDatatbase name: {:?}", &mongodb_name);

                url.set_path("");
                println!("\tUrl: {}", url.to_string());

                observer_settings.mongo_observer = Some(MongoObserverOptions {
                    host: url.to_string(),
                    db_name: mongodb_name
                });
            }

            let config = MultiVicsek::new(Some(config_path));

            let metric_options =  if !all_metrics {
                MetricsOptions{
                    order_parameter: metrics.order_parameter,
                    frequencies: metrics.frequencies,
                    speed_of_rotation: metrics.speed_of_rotation,
                    spacial_entropies: metrics.spacial_entropies,
                    number_of_species_changes: metrics.number_of_species_changes,
                    number_of_rotations: metrics.number_of_rotations,
                    max_periodic,
                    displacement_distance,
                    x_entropies: metrics.x_entropies,
                    species_orientation: metrics.species_orientation,
                    cell_size,
                    y_entropies: metrics.y_entropies,
                }
            } else {
                MetricsOptions {
                    order_parameter: true,
                    frequencies: true,
                    spacial_entropies: true,
                    speed_of_rotation: true,
                    number_of_species_changes:true,
                    number_of_rotations: true,
                    max_periodic,
                    displacement_distance,
                    x_entropies: true,
                    species_orientation: true,
                    cell_size,
                    y_entropies: true,
                }
            };

            let ex_options = ExperimentOptions {
                experiment_name,
                base_dir,
                save_svgs,
                checkpoints,
                run_id,
                vis: VisParams { image_size, vector_length: particle_length, marker_size },
                metric_options,
            };

            multi_experiment(&config, observer_settings, ex_options, &cli.sacred_options);
        }
        Commands::CreateVideo { run_id, ffmpeg_settings, image_name_pattern } => {
            if let Some(mut url) = cli.mongo {
                let mongodb_name = parse_mongo_dbname(&url);
                url.set_path("");

                let mongo_settings = MongoObserverOptions {
                    host: url.to_string(),
                    db_name: mongodb_name,
                };

                process_video(run_id, mongo_settings, image_name_pattern.as_deref(), ffmpeg_settings);
            } else {
                panic!("need mongodb options. -m argument not given.");
            }
        }
    }
}

fn parse_mongo_dbname(url: &Url) -> Option<String> {
    url.path_segments().ok_or_else(|| "cannot be a base url").unwrap().next().and_then(|name| {
        if name.is_empty() {
            return None
        }
        Some(name)
    })
        .map(|name| {
            name.to_string()
        })
}
