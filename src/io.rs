use std::fs::{File};
use std::io::{BufReader, Error, Write};
use std::mem::MaybeUninit;
use std::sync::{Arc};
use std::{mem, thread};
use std::time::Duration;
use ringbuf::{Consumer, SharedRb};
use serde_json::Deserializer;
use crate::settings::Settings;
use crate::vicsek::Observation;
use std::io::Read;
use std::path::{Path, PathBuf};
use serde::{Deserialize, Serialize};
use chrono::{DateTime, Utc};
use fxhash::FxHashMap;
use itertools::Itertools;
use mongodb::bson::oid::ObjectId;
use crate::observers::events::{ExperimentInfo, HostInfo, Info};
use mongodb::bson;


type TConsumer<T> = Consumer<T,Arc<SharedRb<T, Vec<MaybeUninit<T>>>>>;
const SLEEP_TIME_MILLIS: u64 = 100;

pub trait ConsumerHandler<T>: Send + Sync {
    fn handle(&mut self, cons: &mut TConsumer<T>) -> Result<(), Error>;
}

pub struct JsonVicsekWriter<'a> {
    buffer: String,
    file: File,
    settings: &'a Settings
}

impl <'a>JsonVicsekWriter<'a> {
    pub fn new(settings: &'a Settings) -> Self {
        Self {
            buffer: String::new(),
            file: File::options().write(true).truncate(true).create(true).open(Path::new(&settings.output_dir).join(&settings.data_file_name)).unwrap(),
            settings
        }
    }
}

impl <'a> ConsumerHandler<Observation> for JsonVicsekWriter<'a> {

    fn handle<'b>(&mut self, cons: &mut Consumer<Observation, Arc<SharedRb<Observation, Vec<MaybeUninit<Observation>>>>>) -> Result<(), Error> {
        let mut obs_count = 0usize;
        let no_obs = self.settings.vicsek.tmax - self.settings.vicsek.tskip;
        self.buffer.push('[');
        'outer: loop {
            if cons.is_empty() {
                thread::sleep(Duration::from_millis(SLEEP_TIME_MILLIS));
            }
            let obs_iter = cons.pop_iter();
            for obs in obs_iter {
                self.buffer.push_str(&serde_json::to_string(&obs)?);
                if obs_count < no_obs { self.buffer.push(','); }
                obs_count += 1;
            }
            self.file.write_all(self.buffer.as_bytes())?;
            self.buffer.clear();
            if obs_count == no_obs + 1 { break 'outer; }
        }
        self.buffer.push(']');
        self.file.write_all(self.buffer.as_bytes())?;
        Ok(())
    }
}

pub struct ObsFileReader {
    reader: BufReader<File>,
    exhausted: bool
}

impl ObsFileReader {
    pub fn new(data: PathBuf) -> Self {
        Self {
            reader: {
                let mut reader = BufReader::new(File::options().read(true).write(false).create(false).open(data).unwrap());
                let mut skipped = 0u8;
                reader.read_exact(std::slice::from_mut(&mut skipped)).unwrap();
                assert_eq!(skipped, b'[');
                reader
            },
            exhausted: false
        }
    }
}

impl Iterator for ObsFileReader {
    type Item = Observation;

    fn next(&mut self) -> Option<Self::Item> {
        if self.exhausted { return None; }
        let mut skipped = 0u8;
        let obs: Observation = Deserializer::from_reader(&mut self.reader).into_iter().next().ok_or("Expected a JSON Observation value").unwrap().unwrap();

        self.reader.read_exact(std::slice::from_mut(&mut skipped)).unwrap();

        if skipped != b',' {
            let mut leftover_data = vec![b'[', skipped];
            self.reader.read_to_end(&mut leftover_data).unwrap();
            serde_json::from_slice::<[u8; 0]>(&leftover_data).unwrap();
            self.exhausted = true;
            return None;
        }
        Some(obs)
    }
}

struct PairObsFileReader {
    reader: ObsFileReader,
    curr_obs: Option<Observation>,
}

impl PairObsFileReader {
    fn new(data: PathBuf) -> Self {
        let mut reader = ObsFileReader::new(data);
        let first_obs = reader.next();
        Self {
            reader,
            curr_obs: first_obs,
        }
    }
}

impl Iterator for PairObsFileReader {
    type Item = (Option<Observation>, Option<Observation>);

    fn next(&mut self) -> Option<Self::Item> {
        let obs1 = self.reader.next();

        let old = mem::replace(&mut self.curr_obs, obs1);
        old.as_ref()?;
        Some((old, self.curr_obs.clone()))
    }
}

pub trait Syncable {
    fn sync(&self);
}

#[derive(Default, Clone, Serialize, Deserialize, Debug)]
pub(crate) struct MetricsCollection {
    pub(crate) steps: Vec<usize>,
    pub(crate) values: Vec<MetricValues>,
    pub(crate) timestamps: Vec<DateTime<Utc>>
}

pub(crate) type MetricsByName = FxHashMap<String, MetricsCollection>;

pub(crate) fn metrics_by_name(metrics: Vec<Metric>) -> FxHashMap<String, MetricsCollection> {
    FxHashMap::from_iter(metrics.into_iter().into_grouping_map_by(|metric| metric.name.to_owned())
        .fold(MetricsCollection::default(), |mut acc, _, b| {
            acc.values.push(b.value);
            acc.steps.push(b.step);
            acc.timestamps.push(b.timestamp);
            acc
        }))
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Metric {
    pub(crate) name: String,
    pub(crate) step: usize,
    pub(crate) value: MetricValues,
    pub(crate) timestamp: DateTime<Utc>
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum MetricValues {
    Float(f64),
    UInt(u64),
    Usize(usize),
    SInt(i64),
    Char(char),
    Bool(bool),
    String(String),
    Blob(Vec<u8>)
}

impl From<f64> for MetricValues {
    fn from(value: f64) -> Self {
        MetricValues::Float(value)
    }
}

impl From<usize> for MetricValues {
    fn from(value: usize) -> Self {
        MetricValues::Usize(value)
    }
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct Artifact {
    pub name: String,
    pub file_id: ObjectId
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct RunEntry<C, M, R, I>
where
C: Default,
M: Default,
R: Default,
I: Default
{
    #[serde(rename = "_id")]
    pub(crate) _id: Option<i64>,
    experiment: ExperimentInfo,
    command: String,
    host_info: HostInfo,
    #[serde(with = "bson::serde_helpers::chrono_datetime_as_bson_datetime")]
    start_time: DateTime<Utc>,
    config: C,
    meta_info: M,
    resources: Vec<String>,
    pub(crate) artifacts: Vec<Artifact>,
    captured_out: String,
    info: Info<I>,
    #[serde(with = "chrono_datetime_option_as_bson_datetime_option")]
    heartbeat: Option<DateTime<Utc>>,
    #[serde(with = "chrono_datetime_option_as_bson_datetime_option")]
    stop_time: Option<DateTime<Utc>>,
    fail_trace: Option<String>,
    status: RunStatus,
    result: R

}

impl<C, M, R, I> RunEntry<C, M, R, I>
where
C: Default,
M: Default,
R: Default,
I: Default
{
    pub fn new(experiment: ExperimentInfo, command: String, start_time: DateTime<Utc>, config: C) -> Self {
        Self {
            experiment,
            command,
            start_time,
            config,
            ..Default::default()
        }
    }

    pub fn new_started(experiment: ExperimentInfo, command: String, start_time: DateTime<Utc>, config: C, host_info: HostInfo, meta_info: M) -> Self {
        Self {
            experiment,
            command,
            start_time,
            config,
            host_info,
            meta_info,
            status: RunStatus::Running,
            ..Default::default()
        }
    }

    pub fn new_queued(experiment: ExperimentInfo, command: String, config: C, host_info: HostInfo, meta_info: M) -> Self {
        Self {
            experiment,
            command,
            config,
            host_info,
            meta_info,
            status: RunStatus::Queued,
            ..Default::default()

        }
    }

    pub fn create() -> Self {
        Self {
            status: RunStatus::Created,
            ..Default::default()
        }
    }

    pub fn status(&mut self, status: RunStatus) -> &mut Self {
        if let RunStatus::Failed(trace) = &status {
            self.fail_trace = Some(trace.into());
        }
        self.status = status;
        self
    }

    pub fn resources(&self) -> &Vec<String> {
        &self.resources
    }

    pub fn stop_time(&mut self, stop_time: DateTime<Utc>) -> &mut Self {
        self.stop_time = Some(stop_time);
        self
    }

    pub fn heartbeat(&mut self, heartbeat: DateTime<Utc>) -> &mut Self {
        self.heartbeat = Some(heartbeat);
        self
    }

    pub fn captured_out(&mut self, captured_out: String) -> &mut Self {
        self.captured_out = captured_out;
        self
    }

    pub fn meta_info(&mut self, meta_info: M) -> &mut Self {
        self.meta_info = meta_info;
        self
    }

    pub fn host_info(&mut self, host_info: HostInfo) -> &mut Self {
        self.host_info = host_info;
        self
    }

    pub fn add_resource(&mut self, path: String) {
        self.resources.push(path);
    }

    pub fn add_artifact(&mut self, artifact: Artifact) {
        self.artifacts.push(artifact)
    }

    pub fn info(&mut self, info: Info<I>) -> &mut Self {
        self.info = info;
        self
    }

    pub fn get_info(&self) -> &Info<I> {
        &self.info
    }

    pub fn get_info_mut(&mut self) -> &mut Info<I> {
        &mut self.info
    }

    pub fn results(&mut self, results: R) -> &mut Self {
        self.result = results;
        self
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Run {
    pub start_time: Option<DateTime<Utc>>,
    pub stop_time: Option<DateTime<Utc>>,
    pub status: RunStatus,
    pub commit_hash: String,
    pub branch: String,
    pub compiler: String,
    pub version: String,
    pub name: String,
    #[serde(skip)]
    path: String
}

impl Run {

    pub(crate) fn new(path: &str) -> Self {
        Self {
            start_time: None,
            stop_time: None,
            status: RunStatus::Queued,
            commit_hash: built_info::GIT_COMMIT_HASH.unwrap().to_owned(),
            branch: built_info::GIT_HEAD_REF.unwrap().to_owned(),
            compiler: built_info::RUSTC_VERSION.to_owned(),
            version: built_info::PKG_VERSION.to_owned(),
            name: built_info::PKG_NAME.to_owned(),
            path: path.to_owned()
        }
    }

    pub(crate) fn from_file(path: &str) -> Self {
        let file = File::options().read(true).open(path).unwrap();
        serde_json::from_reader(file).unwrap()
    }

    pub(crate) fn run_and_sync(&mut self) {
        self.start_time = Some(Utc::now());
        self.status = RunStatus::Running;
        self.sync();
    }

    pub(crate) fn complete_and_sync(&mut self) {
        self.stop_time = Some(Utc::now());
        self.status = RunStatus::Completed;
        self.sync();
    }

    pub(crate) fn fail_and_sync(&mut self, error_msg: &str) {
        self.stop_time = Some(Utc::now());
        self.status = RunStatus::Failed(error_msg.to_owned());
        self.sync();
    }

    pub(crate) fn queue_and_sync(&mut self) {
        self.stop_time = None;
        self.status = RunStatus::Completed;
        self.sync();
    }
}

impl Syncable for Run {
    fn sync(&self) {
        let mut file = File::options().write(true).truncate(true).create(true).open(&self.path).unwrap();
        file.write_all(serde_json::to_string_pretty(self).unwrap().as_bytes()).unwrap();
    }
}

#[derive(Serialize, Deserialize, Clone, Default, Debug)]
pub enum RunStatus {
    #[default]
    Created,
    Queued,
    Running,
    Completed,
    Failed(String),
    Interrupted,
    TimedOut
}

pub mod built_info {
    // The file has been placed there by the build script.
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

pub mod chrono_datetime_option_as_bson_datetime_option {
    use chrono::Utc;
    use serde::{Deserialize, Deserializer, Serialize, Serializer};
    use std::result::Result;
    use mongodb::bson::{Bson, DateTime};
    use serde::de::Error;

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<chrono::DateTime<Utc>>, D::Error>
        where
            D: Deserializer<'de>,
    {
        match Bson::deserialize(deserializer)? {
            Bson::Null => Ok(None),
            Bson::DateTime(dt) => Ok(Some(dt.to_chrono())),
            _ => Err(D::Error::custom("expecting DateTime or Option<DateTime>")),
        }
    }

    pub fn serialize<S: Serializer>(
        val: &Option<chrono::DateTime<Utc>>,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        match val {
            None => None::<DateTime>.serialize(serializer),
            Some(val) => {
                let datetime = DateTime::from_chrono(val.to_owned());
                datetime.serialize(serializer)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::{Path, PathBuf};
    use assert_approx_eq::assert_approx_eq;
    use rayon::iter::ParallelBridge;
    use crate::io::{ObsFileReader, PairObsFileReader};
    use rayon::iter::ParallelIterator;

    #[test]
    fn test_iterator() {
        let reader = ObsFileReader::new(Path::new("/home/max/Downloads/vicsek/Rotate_90/run13/data.json").to_path_buf());
        reader.par_bridge().for_each(|obs| {
            println!("obs step: {}", obs.step);
        });
    }

    #[test]
    fn test_pair_iterator() {
        let reader = PairObsFileReader::new(Path::new("/home/max/Downloads/vicsek/Rotate_90/run13/data.json").to_path_buf());
        for obs in reader {
            let (obs1, obs2) = obs;
            let v1 = obs1.map_or(String::from("-1"), |obs| {
                format!("step: {}", obs.step)
            });
            let v2 = obs2.map_or(String::from("-1"), |obs| {
                format!("step: {}", obs.step)
            });
            println!("({}, {})", v1, v2);
        }
    }

    #[test]
    fn test_obs_fn() {
        let mut reader = ObsFileReader::new(PathBuf::from("fixtures/default_run/data.json"));
        let obs =  reader.next().unwrap();
        let v = obs.velocities(0.03);
        v.rows().into_iter().for_each(|v| {
            let len_v = v.map(|xy| xy.powi(2)).sum().sqrt();
            assert_approx_eq!(len_v, 0.03);
        });
    }
}
