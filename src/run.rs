use std::io::Write;
use std::path::{PathBuf};
use std::process::Stdio;
use std::sync::{Arc, RwLock};
use std::thread;
use chrono::Utc;
use clap::{Args};
use crossbeam::channel::TryRecvError;
use futures::{TryStreamExt};
use mongodb::bson::{doc, Document, Regex};
use mongodb::Client;
use mongodb::options::{FindOptions};
use tokio::runtime::{Builder, Runtime};
use crate::io::{Metric, metrics_by_name, RunEntry};
use crate::observers::events::{Completed, ExperimentInfo, Heartbeat, HostInfo, LogMetrics, ObserverEvents, Repository};
use crate::observers::{Observers};
use crate::observers::mongodb::{SyncMongoObserver};
use crate::settings;
use crate::settings::Vicsek;
use crate::vicsek::{MetaData, ParameterPerSpecies, VicsekMongoSimMgr};
use ffmpeg_cli::{FfmpegBuilder, Parameter};
use fxhash::FxHashMap;
use plotters::style::RGBColor;
use serde::{Deserialize, Serialize};
use timer::Timer;
use crate::cli::SacredOptions;
use crate::vis::{ VisParams};
use crate::io::built_info;

pub(crate) type Event = ObserverEvents<settings::Vicsek, MetaData, VicsekResult, (), VicsekInfo>;
pub(crate) type ArcEvent = Arc<ObserverEvents<settings::Vicsek, MetaData, VicsekResult, (), VicsekInfo>>;
pub(crate) type VicsekRunEntry = RunEntry<settings::Vicsek, MetaData, VicsekResult, VicsekInfo>;
pub(crate) type VicsekObservers = Observers<Vicsek, MetaData, VicsekResult, (), VicsekInfo>;
pub(crate) type VicsekMongoObserver = SyncMongoObserver<Vicsek, MetaData, VicsekResult, (), VicsekInfo>;

#[derive(Default, Serialize, Deserialize, Clone, Debug)]
pub struct VicsekInfo {
    pub density: Option<f64>,
    pub cell_size: Option<f64>,
    pub svg_dir: Option<String>,
    pub spacial_entropy_max: Option<f64>,
    pub species_colors: Option<FxHashMap<String, String>>,
    pub extinct_species: FxHashMap<String, usize>,
    pub displacement_distance: Option<f64>,
    pub max_periodic: Option<usize>
}

impl VicsekInfo {
    pub fn set_density(&mut self, density: f64) {
        self.density = Some(density);
    }

    pub fn set_cell_size(&mut self, cell_size: f64) {
        self.cell_size = Some(cell_size);
    }

    pub fn set_svg_dir(&mut self, svg_dir: String) {
        self.svg_dir = Some(svg_dir);
    }

    pub fn set_spacial_entropies_max(&mut self, spacial_entropy_max: f64) {
        self.spacial_entropy_max = Some(spacial_entropy_max);
    }
    
    pub fn set_species_colors(&mut self, species_colors: FxHashMap<String, String>) {
        self.species_colors = Some(species_colors);
    }
}

#[derive(Default, Serialize, Deserialize, Clone, Debug)]
pub struct VicsekResult {}

pub struct ExperimentOptions {
    pub experiment_name: String,
    pub base_dir: Option<PathBuf>,
    pub save_svgs: bool,
    pub checkpoints: bool,

    pub run_id: Option<i64>,

    pub vis: VisParams,

    pub metric_options: MetricsOptions,
}

pub fn run(config: Vicsek, observers: Vec<VicsekObservers>, ex_options: &ExperimentOptions, sacred_options: &SacredOptions)
{
    let vicsek = crate::vicsek::Vicsek::new(&config);

    let vicsek_result = Arc::new(RwLock::new(VicsekResult::default()));
    let vicsek_info = Arc::new(RwLock::new(VicsekInfo::default()));

    let (bc_sender, bc_receiver) = crossbeam::channel::bounded(100);
    let (heartbeat_sender, heartbeat_receiver) = crossbeam::channel::bounded::<Vec<Metric>>(sacred_options.hb_channel_capacity);
    let (close_timer_sender, close_sender_receiver) = crossbeam::channel::bounded(1);

    let run_bc_sender = bc_sender.clone();

    let timer = Timer::new();

    let bc_tx_heartbeat = bc_sender.clone();
    let vicsek_result_scheduler = vicsek_result.clone();
    let vicsek_info_scheduler = vicsek_info.clone();
    let scheduler = timer.schedule_repeating(chrono::Duration::seconds(sacred_options.hb_frequency), move || {
        let result = heartbeat_receiver.try_recv();
        match result {
            Ok(mut metrics) => {
                let mut remaining_metrics: Vec<Metric> = heartbeat_receiver.try_iter().flatten().collect();
                metrics.append(&mut remaining_metrics);

                if !metrics.is_empty() {
                    let named_metrics = metrics_by_name(metrics);

                    bc_tx_heartbeat.send(Arc::new(Event::LogMetrics(LogMetrics {
                        metrics: named_metrics,
                        info: vicsek_info_scheduler.read().unwrap().to_owned(),
                    }))).unwrap();
                }
            }
            Err(error) => {
                match error {
                    TryRecvError::Empty => {}
                    TryRecvError::Disconnected => {
                        // all metrics are written send signal to shutdown the scheduler
                        close_timer_sender.send(()).unwrap();
                    }
                }
            }
        }

        bc_tx_heartbeat.send(Arc::new(Event::Heartbeat(Heartbeat{
            captured_out: "".to_string(),
            beat_time: Utc::now(),
            result: vicsek_result_scheduler.read().unwrap().to_owned(),
            info: vicsek_info_scheduler.read().unwrap().to_owned(),
        }))).unwrap_or(());
    });

    let mut spawners = Vec::default();
    for observer in observers {
        spawners.push(observer.start(bc_receiver.clone()))
    }

    drop(bc_receiver);

    let run = VicsekMongoSimMgr::run(bc_sender, heartbeat_sender, config.to_owned(), ExperimentInfo {
        name: ex_options.experiment_name.to_owned(),
        base_dir: ex_options.base_dir.to_owned(),
        sources: None,
        dependencies: None,
        repositories: Some(vec![Repository {
            url: built_info::GIT_HEAD_REF.unwrap().to_string(),
            commit: built_info::GIT_COMMIT_HASH.unwrap().to_string(),
            dirty: built_info::GIT_DIRTY.unwrap(),
        }]),
        main_file: None,
    }, HostInfo {
        host: None,
        rust_version: None,
        os: None,
        cpu: None,
        gpu: None,
    }, MetaData {
        command: "vicsek".to_string(),
    }, ex_options.run_id,
    vicsek,
    &ex_options,
    vicsek_info);

    // simulation done. Collect remaining Metrics in the buffer and stop the timer
    close_sender_receiver.recv().unwrap();
    drop(scheduler);

    let event = Arc::new(Event::Completed(Completed {
        stop_time: Utc::now(),
        result: vicsek_result.read().unwrap().to_owned(),
    }));

    let final_call = run_bc_sender.send(event);

    match final_call {
        Ok(_) => {
            // shutdown sender
            drop(run_bc_sender);
        }
        Err(error) => {
            // log error and shutdown sender
            drop(run_bc_sender);
        }
    }
    spawners.into_iter().for_each(|spawner| spawner.join().unwrap());
    println!("all observers done.");
}

pub struct MongoObserverOptions {
    pub(crate) host: String,
    pub(crate) db_name: Option<String>
}

impl MongoObserverOptions {
    fn connect_sync(&self) -> mongodb::sync::Client {
        mongodb::sync::Client::with_uri_str(&self.host).unwrap()
    }

    fn connect_blocking(&self, rt: &Runtime) -> Client {
        rt.block_on(Client::with_uri_str(&self.host)).unwrap()
    }

    async fn connect(&self) -> Client {
        Client::with_uri_str(&self.host).await.unwrap()
    }
}

impl Default for MongoObserverOptions {
    fn default() -> Self {
        Self {
            host: "mongodb://localhost:27017".to_string(),
            db_name: Some("sacred".to_string())
        }
    }
}

pub struct MetricsOptions {
    pub order_parameter: bool,
    pub frequencies: bool,
    pub speed_of_rotation: bool,
    pub spacial_entropies: bool,
    pub species_orientation: bool,
    pub number_of_species_changes: bool,
    pub number_of_rotations: bool,
    pub max_periodic: usize,
    pub displacement_distance: f64,
    pub x_entropies: bool,
    pub y_entropies: bool,
    pub cell_size: f64
}

#[derive(Default)]
pub struct ObserverOptions {
    pub(crate) mongo_observer: Option<MongoObserverOptions>,
}

#[derive(Clone)]
pub enum EitherIter<I1, I2>
where
    I1: Iterator,
    I2: Iterator
{
    Either(I1),
    Or(I2)
}

impl<I1, I2> Iterator for EitherIter<I1, I2>
where
    I1: Iterator,
    I2: Iterator<Item = I1::Item>
{
    type Item = I1::Item;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            EitherIter::Either(iter) => { iter.next() }
            EitherIter::Or(iter) => { iter.next() }
        }
    }
}

pub fn multi_experiment(multi_config: &settings::MultiVicsek, observer_options: ObserverOptions, ex_options: ExperimentOptions, sacred_options: &SacredOptions)
{
    let rt = Arc::new(Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap());

    let mongo_client = observer_options.mongo_observer.map(|settings| {
        (settings.connect_sync(), settings.db_name.unwrap_or(MongoObserverOptions::default().db_name.unwrap()))
    });

    for config in multi_config.to_owned().into_iter() {
        let mut observers = Vec::default();

        if let Some((client, db_name)) = &mongo_client {
            observers.push(Observers::SyncMongoDB(VicsekMongoObserver::builder().client(client.clone()).db_name(db_name.to_string()).build()))
        }

        run(config, observers, &ex_options, sacred_options);
    }
}

#[derive(Args)]
pub struct FFmpegSettings {
    #[arg(default_value_t = 30, long)]
    framerate: u8,
    #[arg(long)]
    image_size: u32,
    #[arg(long)]
    vaapi_device: Option<PathBuf>,
    #[arg(long)]
    vcodec: Option<String>,
    #[arg(long)]
    vf: Option<String>,
    #[arg(long)]
    out: PathBuf,
    #[arg(long)]
    cv: Option<String>,
    #[arg(long)]
    out_f: Option<String>

}

pub fn process_video(run_id: i64, db_options: MongoObserverOptions, image_name_pat: Option<&str>, ffmpeg: FFmpegSettings) {
    let rt = Arc::new(Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap());

    rt.block_on(async {
        let mongo_client = db_options.connect().await;
        let db_name = db_options.db_name.unwrap_or("sacred".to_string());

        let db = mongo_client.database(&db_name);

        let data_collection = db.collection::<Document>("data");
        let regex = Regex{
            pattern: "pic\\d+.svg".to_string(), // format!("artifact://{}/{}/{}", "runs", run_id, image_name_pat.unwrap_or("pic\\d+.svg")),
            options: "s".to_string(),
        };
        let mut svgs = data_collection.find(doc! {"name": regex, "run_id": run_id}, FindOptions::builder().sort(doc! {"name": 1}).build()).await.unwrap();
        // let mut svgs = fs.find(doc! {"filename": regex}, GridFsFindOptions::builder().sort(doc! {"filename": 1}).build()).await.unwrap();

        let binding = ffmpeg.framerate.to_string();
        let s = format!("{}x{}", ffmpeg.image_size, ffmpeg.image_size);
        let mut ff_builder = FfmpegBuilder::new()
            .option(Parameter::Single("y"))
            .input(ffmpeg_cli::File::new("-")
                .option(Parameter::KeyValue("r", &binding))
                .option(Parameter::KeyValue("f", "rawvideo"))
                .option(Parameter::KeyValue("pix_fmt", "rgba"))
                .option(Parameter::KeyValue("s", &s)));


        ff_builder = ff_builder
            .stdin(Stdio::piped())
            .stderr(Stdio::inherit())
            .stdout(Stdio::inherit());

        if let Some(vaapi_device) = &ffmpeg.vaapi_device {
            ff_builder = ff_builder.option(Parameter::KeyValue("vaapi_device", vaapi_device.to_str().unwrap()))
        }

        let vcodec = ffmpeg.vcodec.unwrap_or("libx264".to_string());
        let mut output_file = ffmpeg_cli::File::new(ffmpeg.out.to_str().unwrap())
            .option(Parameter::KeyValue("vcodec", &vcodec));
        if let Some(vf) = &ffmpeg.vf {
            output_file = output_file.option(Parameter::KeyValue("vf", vf));
        }
        if let Some(cv) = &ffmpeg.cv {
            output_file = output_file.option(Parameter::KeyValue("c:v", cv));
        }
        if let Some(f) = &ffmpeg.out_f {
            output_file = output_file.option(Parameter::KeyValue("f", f));
        }

        ff_builder = ff_builder.output(output_file);
        let mut cmd = ff_builder.to_command();
        let mut child = cmd.spawn().unwrap();

        let mut stdin = child.stdin.take().expect("Failed to get stdin");

        let (tx, mut rx) = tokio::sync::mpsc::channel::<Vec<u8>>(100);

        thread::spawn(move || {
            while let Some(svg) = rx.blocking_recv() {
                let svg_string = String::from_utf8(svg).unwrap();
                let svg_img = nsvg::parse_str(&svg_string, nsvg::Units::Pixel, 96.0).unwrap();
                let image = svg_img.rasterize(1.0).unwrap();

                stdin.write_all(&image.into_raw()).unwrap();
            }
        });

        while let Some(svg) = svgs.try_next().await.unwrap() {
            let svg_string = svg.get_str("value").unwrap().to_string();

            tx.send(svg_string.into_bytes()).await.unwrap();
        }
        drop(tx);

        let r = child.wait().unwrap();

    });

}

#[cfg(test)]
mod test {
    use std::path::{Path, PathBuf};
    use crate::run::{FFmpegSettings, MongoObserverOptions, process_video};
    use crate::settings::iterconfig::{generate_float_values, generate_species_amounts};
    use crate::vicsek::MetaData;


}