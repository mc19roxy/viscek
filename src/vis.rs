use std::error::Error;
use ndarray::{Array1, Array2, azip, IntoNdProducer};
use crate::settings::{Visualisation};
use plotters::prelude::*;
use plotters::coord::types::RangedCoordf32;
use std::f32;
use std::sync::Arc;
use plotters::style::full_palette::{INDIGO_700, PURPLE, RED_A700};
use serde::{Serialize, Serializer};
use crate::vicsek::Observation;


type VicsekArea = Cartesian2d<RangedCoordf32, RangedCoordf32>;

pub struct RenderParams {
    pub length: u16,
    pub tmax: usize
}

pub struct VisParams {
    pub image_size: u32,
    pub vector_length: u32,
    pub marker_size: u32
}

pub trait Render<P, B, V> {
    fn render(&self, vis_settings: &V, params: &P) -> Result<B, Box<dyn NewTrait>>;
}

impl Render<Arc<RenderParams>, String, Arc<VisParams>> for Observation {

    fn render(&self, vis_settings: &Arc<VisParams>, params: &Arc<RenderParams>) -> Result<String, Box<dyn NewTrait>> {
        let mut buf = String::new();

        let positions = &self.positions;
        let thetas = &self.thetas;
        let spins = &self.spins;

        let lxy = params.length as f32 / 2f32;
        let image_size = vis_settings.image_size;

        {
            let root = SVGBackend::with_string(&mut buf, (image_size, image_size)).into_drawing_area();

            let root = root.apply_coord_spec(Cartesian2d::<RangedCoordf32, RangedCoordf32>::new(
                -lxy..lxy,
                lxy..-lxy,
                (0..image_size as i32, 0..image_size as i32)
            ));

            root.fill(&WHITE).unwrap();

            let vector_scale = vis_settings.vector_length as f32 * (params.length as f32 / image_size as f32);

            draw_particles(&root, positions, thetas, spins, vector_scale, lxy, vis_settings.marker_size);
        }

        Ok(buf)
    }
}

impl Render<RenderParams, (), Visualisation> for Observation {

    fn render(&self, vis_settings: &Visualisation, params: &RenderParams) -> Result<(), Box<dyn NewTrait>> {
        let positions = &self.positions;
        let thetas = &self.thetas;
        let step = self.step;
        let spins = &self.spins;

        let prefix = (params.tmax.ilog10() + 1) as usize;

        let vis_dir = vis_settings.vis_dir().join(format!("pic{step:0>prefix$}.svg"));

        let lxy = params.length as f32 / 2f32;
        let image_size = vis_settings.image_size;

        {
            let root = SVGBackend::new(&vis_dir, (image_size, image_size)).into_drawing_area();

            let root = root.apply_coord_spec(Cartesian2d::<RangedCoordf32, RangedCoordf32>::new(
                -lxy..lxy,
                lxy..-lxy,
                (0..image_size as i32, 0..image_size as i32)
            ));

            root.fill(&WHITE).unwrap();

            let vector_scale = vis_settings.vector_length as f32 * (params.length as f32 / image_size as f32);

            draw_particles(&root, positions, thetas, spins, vector_scale, lxy, 1);
        }

        Ok(())
    }
}

fn draw_particles(root: &DrawingArea<impl DrawingBackend, VicsekArea>, positions: &Array2<f64>, thetas: &Array1<f64>, spins: &Array1<i8>, scale: f32, lxy: f32, marker_size: u32) {
    azip!((position in positions.rows().into_producer(), theta in thetas, spin in spins) {
                let dx = -(scale * theta.cos() as f32).clamp(-lxy, lxy);
                let dy = -(scale * theta.sin() as f32).clamp(-lxy, lxy);
                let x = position[0] as f32;
                let y = position[1] as f32;
                let color = species_to_color(*spin);
                root.draw(&Cross::new((x, y), marker_size, ShapeStyle::from(color).filled())).unwrap();
                root.draw(&PathElement::new(vec![(x, y), (x + dx, y + dy)], ShapeStyle::from(color).filled())).unwrap();
            });
}

pub(crate) fn species_to_color(species: i8) -> RGBColor {
    match species {
        -2 => { GREEN }
        -1 => { PURPLE }
        0 => { BLACK }
        1 => { RED }
        2 => { BLUE }
        3 => { YELLOW }
        _ => { WHITE }
    }
}

struct SpeciesRGB(RGBColor);

impl From<i8> for SpeciesRGB {
    fn from(value: i8) -> Self {
        let red = (value as u8 >> 5) * 32;
        let green = ((value as u8 & 28) >> 2) * 32;
        let blue = (value as u8 & 3) * 64;
        Self(RGBColor(red, green, blue))
    }
}

pub trait NewTrait: Error + Send + Sync {
    fn box_clone(&self) -> Box<dyn NewTrait>;
}

impl Clone for Box<dyn NewTrait> {
    fn clone(&self) -> Self {
        self.box_clone()
    }
}

impl Serialize for dyn NewTrait {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        let message = format!("{{ description: {} }}", self);

        message.serialize(serializer)
    }
}

#[cfg(test)]
mod tests {
    use plotters::prelude::*;
    use plotters::coord::types::RangedCoordf32;

    #[test]
    fn plotters_test() -> Result<(), Box<dyn std::error::Error>> {
        let root = SVGBackend::new("test.svg", (600, 600)).into_drawing_area();

        let root = root.apply_coord_spec(Cartesian2d::<RangedCoordf32, RangedCoordf32>::new(
            0f32..7f32,
            0f32..7f32,
            (0..600, 0..600)
        ));

        // let angle: f32 = 3.6652;
        let angle: f32 = -1. * std::f32::consts::PI;
        let dx = angle.cos();
        let dy = angle.sin();

        let dot_and_label = |x: f32, y: f32| {
            return EmptyElement::at((x, y))
                + Cross::new((0,0), 3, ShapeStyle::from(&BLACK).filled())
                + Text::new(
                        format!("({:.2},{:.2})", x, y),
                        (10, -10),
                        ("sans-serif", 15.).into_font(),
                    );
        };

        root.draw(&dot_and_label(7., 7.))?;
        root.draw(&PathElement::new(vec![(2., 1.5), (2.+dx, 1.5+dy)], ShapeStyle::from(&BLACK).filled()))?;
        root.draw(&dot_and_label(2., 1.5))?;
        root.draw(&dot_and_label(6., 3.4))?;
        root.present()?;

        Ok(())
    }
}

trait Plotabale {
    fn plot(&self);
}
