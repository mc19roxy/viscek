pub(crate) mod adapters;
pub(crate) mod iterconfig;

use config::{Config, ConfigError, Environment};
use serde::{Deserialize, Serialize};
use std::{env, path::Path};
use std::collections::HashMap;
use std::fs::File;
use std::iter::{Map, Once, once, Step, StepBy};
use std::ops::{RangeInclusive};
use std::path::PathBuf;
use std::str::FromStr;
use clap::{Args, ValueEnum};
use fxhash::FxHashSet;
use itertools::{Itertools, Product};
use ndarray::{array, Array2};
use ron::error::SpannedError;
use crate::run::EitherIter;
use crate::settings::adapters::species_amounts::SubsetSum;
use crate::settings::iterconfig::{FloatStepBy, generate_species_amounts};

#[derive(Debug, Deserialize, Clone, ValueEnum)]
pub enum FileType {
    Json,
    Csv,
    Svg
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde[default]]
pub struct Settings {
    pub output_dir: PathBuf,
    #[serde(default = "default_data_file_name")]
    pub data_file_name: String,
    #[serde(default = "default_run_file_name")]
    pub run_file_name: String,
    #[serde(default = "default_config_file_name")]
    pub config_file_name: String,
    #[serde(default)]
    pub vicsek: Vicsek,
}

fn default_data_file_name() -> String {
        Settings::default().data_file_name
    }
fn default_run_file_name() -> String {
        Settings::default().run_file_name
    }
fn default_config_file_name() -> String {
        Settings::default().config_file_name
    }

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Vicsek {
    #[serde(default = "default_N")]
    pub N: usize,
    #[serde(default = "default_r")]
    pub r: f64,
    #[serde(default = "default_L")]
    pub L: u16,
    #[serde(default = "default_noise_range")]
    pub noise_range: f64,
    #[serde(default = "default_eta")]
    pub eta: f64,
    #[serde(default = "default_v")]
    pub v: f64,
    #[serde(default = "default_tmax")]
    pub tmax: usize,
    #[serde(default = "default_tskip")]
    pub tskip: usize,
    #[serde(default = "default_eta2")]
    pub eta2: f64,
    #[serde(default = "default_filter_bubble_angle")]
    pub filter_bubble_angle: Option<usize>,
    #[serde(default = "default_view_ahead_only")]
    pub view_ahead_only: bool,
    #[serde(default = "default_threshold")]
    pub threshold: usize,
    #[serde(default = "default_eta_spin")]
    pub eta_spin: f64,
    #[serde(default = "default_groups")]
    pub groups: HashMap<String, (usize, Option<UpdateStrategy>)>,
    #[serde(default = "default_group_alignments")]
    pub group_alignments: Vec<Alignment>
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct MultiVicsek {
    #[serde(default = "default_N")]
    pub N: usize,
    #[serde(default = "default_r")]
    pub r: f64,
    #[serde(default = "default_L_multi")]
    pub L: SingleOrMultipleValues<u16, RangeOptions<u16>>,
    #[serde(default = "default_noise_range")]
    pub noise_range: f64,
    #[serde(default = "default_eta_multi")]
    pub eta: SingleOrMultipleValues<f64, FloatStepByOptions>,
    #[serde(default = "default_v")]
    pub v: f64,
    #[serde(default = "default_tmax")]
    pub tmax: usize,
    #[serde(default = "default_tskip")]
    pub tskip: usize,
    #[serde(default = "default_eta2")]
    pub eta2: f64,
    #[serde(default = "default_filter_bubble_angle")]
    pub filter_bubble_angle: Option<usize>,
    #[serde(default = "default_view_ahead_only")]
    pub view_ahead_only: bool,
    #[serde(default = "default_threshold")]
    pub threshold: usize,
    #[serde(default = "default_eta_spin_multi")]
    pub eta_spin: SingleOrMultipleValues<f64, FloatStepByOptions>,
    #[serde(default = "default_groups_multi")]
    pub groups: SingleOrMultipleValues<SpeciesMap, SpeciesCombinatorOptions>,
    #[serde(default = "default_group_alignments")]
    pub group_alignments: Vec<Alignment>
}

type SpeciesMap = HashMap<String, (usize, Option<UpdateStrategy>)>;

#[derive(Deserialize, Clone, Debug, Serialize)]
pub enum SingleOrMultipleValues<V, G>
where
G: IntoIterator<Item = V>
{
    Single(V),
    Multiple(G)
}

impl IntoIterator for SingleOrMultipleValues<SpeciesMap, SpeciesCombinatorOptions>
{
    type Item = SpeciesMap;
    type IntoIter = EitherIter<Once<SpeciesMap>, SpeciesCombinator>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            SingleOrMultipleValues::Single(value) => { EitherIter::Either(once(value)) }
            SingleOrMultipleValues::Multiple(values) => { EitherIter::Or(values.into_iter() )}
        }
    }
}

impl IntoIterator for SingleOrMultipleValues<f64, FloatStepByOptions> {
    type Item = f64;
    type IntoIter = EitherIter<Once<f64>, FloatStepBy>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            SingleOrMultipleValues::Single(value) => { EitherIter::Either(once(value))}
            SingleOrMultipleValues::Multiple(values) => {EitherIter::Or(values.into_iter())}
        }
    }
}

#[derive(Clone, Deserialize, Debug, Serialize)]
pub struct RangeOptions<T> {
    start: T,
    end: T,
    step: usize
}

impl <T> IntoIterator for RangeOptions<T>
where T: Step
{
    type Item = T;
    type IntoIter = StepBy<RangeInclusive<T>>;

    fn into_iter(self) -> Self::IntoIter {
        (self.start..=self.end).step_by(self.step)
    }
}

impl <T> IntoIterator for SingleOrMultipleValues<T, RangeOptions<T>>
where T: Step
{
    type Item = T;
    type IntoIter = EitherIter<Once<T>, StepBy<RangeInclusive<T>>>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            SingleOrMultipleValues::Single(single) => { EitherIter::Either(once(single))}
            SingleOrMultipleValues::Multiple(multi) => {EitherIter::Or(multi.into_iter())}
        }
    }
}


#[derive(Deserialize, Clone, Debug, Serialize)]
pub struct FloatStepByOptions {
    start: f64,
    end: f64,
    step: f64,
    precision: Option<u8>
}

impl IntoIterator for FloatStepByOptions {
    type Item = f64;
    type IntoIter = FloatStepBy;

    fn into_iter(self) -> Self::IntoIter {
        FloatStepBy::new(self.start, self.end, self.step, self.precision)
    }
}

#[derive(Clone, Deserialize, Debug, Serialize)]
pub struct SpeciesOptions {
    name: u8,
    strategy: Option<UpdateStrategy>,
    amount_step: Option<usize>,
    amount_less_than: Option<usize>,
    amount_greater_than: Option<usize>
}

#[derive(Clone, Deserialize, Debug, Serialize)]
pub struct SpeciesCombinatorOptions {
    total: usize,
    species: Vec<SpeciesOptions>,
}

impl IntoIterator for SpeciesCombinatorOptions {
    type Item = SpeciesMap;
    type IntoIter = SpeciesCombinator;

    fn into_iter(self) -> Self::IntoIter {
        SpeciesCombinator::new(self)
    }
}

#[derive(Clone)]
pub struct SpeciesCombinator {
    iter: SubsetSum<Box<dyn FnClone<Output = bool>>>,
    options: SpeciesCombinatorOptions
}

trait FnClone: FnMut(&[usize]) -> bool {
    fn clone_box(&self) -> Box<dyn FnClone<Output=bool>>;
}

impl<T> FnClone for T
    where
        T: 'static + FnMut(&[usize]) -> bool + Clone,
{
    fn clone_box(&self) -> Box<dyn FnClone<Output = bool>> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn FnClone<Output = bool>> {
    fn clone(&self) -> Self {
        (**self).clone_box()
    }
}

impl SpeciesCombinator
{
    pub fn new(options: SpeciesCombinatorOptions) -> Self {
        let species = options.species.to_owned();
        Self {
            iter: generate_species_amounts(options.total, options.species.len(), Box::new(move |v: &[usize]| {
                species.iter().enumerate().fold(true, |mut b, (i, s)| {
                    b &=  s.amount_less_than.map(|a| { v[i] <= a }).unwrap_or(true)
                        && s.amount_step.map(|step| { v[i] % step == 0 }).unwrap_or(true)
                        && s.amount_greater_than.map(|b| { v[i] >= b }).unwrap_or(true);

                    b
                })
            })),
            options
        }
    }
}

impl Iterator for SpeciesCombinator
{
    type Item = HashMap<String, (usize, Option<UpdateStrategy>)>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|v| {
            v.iter().zip(self.options.species.iter()).fold(HashMap::default(), |mut map, (amount, options)| {
                map.insert(options.name.to_string(), (*amount, options.strategy.to_owned()));
                map
            })
        })
    }
}

impl IntoIterator for MultiVicsek {
    type Item = Vicsek;
    type IntoIter = Map<Product<Product<Product<EitherIter<Once<f64>, FloatStepBy>, EitherIter<Once<SpeciesMap>, SpeciesCombinator>>, EitherIter<Once<u16>, StepBy<RangeInclusive<u16>>>>, EitherIter<Once<f64>, FloatStepBy>>, Box<dyn FnMut((((f64, SpeciesMap), u16), f64)) -> Vicsek>>;

    fn into_iter(self) -> Self::IntoIter {
        self.eta
            .into_iter()
            .cartesian_product(self.groups.into_iter())
            .cartesian_product(self.L)
            .cartesian_product(self.eta_spin)
            .map(Box::new(move |(((eta, species), L), eta_spin)| {
            Vicsek {
                N: self.N,
                r: self.r,
                L,
                noise_range: self.noise_range,
                eta,
                v: self.v,
                tmax: self.tmax,
                tskip: self.tskip,
                eta2: self.eta2,
                filter_bubble_angle: self.filter_bubble_angle,
                view_ahead_only: self.view_ahead_only,
                threshold: self.threshold,
                eta_spin,
                groups: species,
                group_alignments: self.group_alignments.to_owned(),
            }
        }))
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum UpdateStrategy {
    Majority(Option<FxHashSet<i8>>),
    MajorityWithThreshold(f64, Option<FxHashSet<i8>>),
    Minority(i8)
}

#[derive(Deserialize, Serialize)]
pub struct Group(f64, Option<UpdateStrategy>);

impl FromStr for UpdateStrategy {
    type Err = SpannedError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        ron::from_str(s)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Alignment(i8, i8, Option<GroupAlignment>);

impl Default for Alignment {
    fn default() -> Self {
        Self(1, 1, None)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum GroupAlignment {
    Align(f64),
    AlignAndRotate(f64, i16),
    Rotate(i16)
}

impl From<&GroupAlignment> for Array2<f64> {
    fn from(value: &GroupAlignment) -> Self {
        match value {
            GroupAlignment::Align(alignment) => array![[*alignment, 0.]],
            GroupAlignment::AlignAndRotate(alignment, rotation) => array![[*alignment, (*rotation as f64).to_radians()]],
            GroupAlignment::Rotate(rotation) => array![[0., (*rotation as f64).to_radians()]]
        }
    }
}

impl From<&GroupAlignment> for (f64, f64) {
    fn from(value: &GroupAlignment) -> Self {
        match value {
            GroupAlignment::Align(alignment)=> (*alignment, 0.),
            GroupAlignment::AlignAndRotate(alignment, rotation) => (*alignment, (*rotation as f64).to_radians()),
            GroupAlignment::Rotate(rotation) => (1., (*rotation as f64).to_radians())
        }
    }
}

impl From<Alignment> for ((i8, i8), Option<GroupAlignment>) {
    fn from(value: Alignment) -> Self {
        ((value.0, value.1), value.2)
    }
}

impl FromStr for GroupAlignment {
    type Err = SpannedError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        ron::from_str(s)
    }
}

impl From<Vicsek> for MultiVicsek {
    fn from(value: Vicsek) -> Self {
        Self {
            N: value.N,
            r: value.r,
            L: SingleOrMultipleValues::Single(value.L),
            noise_range: value.noise_range,
            eta: SingleOrMultipleValues::Single(value.eta),
            tskip: value.tskip,
            eta2: value.eta2,
            filter_bubble_angle: value.filter_bubble_angle,
            view_ahead_only: value.view_ahead_only,
            threshold: value.threshold,
            eta_spin: SingleOrMultipleValues::Single(value.eta_spin),
            groups: SingleOrMultipleValues::Single(value.groups),
            tmax: value.tmax,

            v: value.v,
            group_alignments: value.group_alignments,
        }
    }
}

impl Default for Vicsek {
    fn default() -> Self {
        let n: usize = 300;
        Self {
            N: n,
            r: 1.,
            L: 7,
            tskip: 0,
            tmax: 200,
            v: 0.03,
            noise_range: 0.5, // f64::PI = 3.14159265358979323846264338327950288
            eta: 2.0,

            // Oezhans additions
            eta2: 0.1,
            view_ahead_only: false,
            filter_bubble_angle: None,
            threshold: 0,

            // multi species vicsek model
            eta_spin: 0.05f64,
            groups: {
                HashMap::from_iter([("1".to_string(), (n, None))])
            },
            group_alignments: {
                vec![Alignment(1, 1, Some(GroupAlignment::Align(1.)))]
            },
        }
    }
}

fn default_N() -> usize {
    Vicsek::default().N
}
fn default_r() -> f64 {
    Vicsek::default().r
}
fn default_L() -> u16 {
    Vicsek::default().L
}
fn default_L_multi() -> SingleOrMultipleValues<u16, RangeOptions<u16>> {
    SingleOrMultipleValues::Single(Vicsek::default().L)
}
fn default_noise_range() -> f64 {
    Vicsek::default().noise_range
}
fn default_eta() -> f64 {
    Vicsek::default().eta
}
fn default_eta_multi() -> SingleOrMultipleValues<f64, FloatStepByOptions> {
    SingleOrMultipleValues::Single(Vicsek::default().eta)
}
fn default_eta_spin_multi() -> SingleOrMultipleValues<f64, FloatStepByOptions> {
    SingleOrMultipleValues::Single(Vicsek::default().eta_spin)
}
fn default_v() -> f64 {
    Vicsek::default().v
}
fn default_tskip() -> usize {
    Vicsek::default().tskip
}
fn default_tmax() -> usize {
    Vicsek::default().tmax
}
fn default_eta2() -> f64 {
    Vicsek::default().eta2
}
fn default_view_ahead_only() -> bool {
    Vicsek::default().view_ahead_only
}
fn default_filter_bubble_angle() -> Option<usize> {
    Vicsek::default().filter_bubble_angle
}
fn default_threshold() -> usize {
    Vicsek::default().threshold
}
fn default_eta_spin() -> f64 {
    Vicsek::default().eta_spin
}
fn default_groups() -> HashMap<String, (usize, Option<UpdateStrategy>)> {
    Vicsek::default().groups
}
fn default_groups_multi() -> SingleOrMultipleValues<SpeciesMap, SpeciesCombinatorOptions> {
    SingleOrMultipleValues::Single(Vicsek::default().groups)
}
fn default_group_alignments() -> Vec<Alignment> {
    Vicsek::default().group_alignments
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            output_dir: PathBuf::from("~/vicsek/run"),
            data_file_name: String::from("data.json"),
            run_file_name: String::from("run.json"),
            config_file_name: String::from("config.ron"),
            vicsek: Default::default(),
        }
    }
}

#[derive(Debug, Deserialize, Clone, Args, Serialize)]
pub struct Visualisation {
    pub input: PathBuf,
    #[arg[default_value_t = String::from("data.json"), long]]
    pub data_file_name: String,
    #[arg[default_value_t = String::from("run.json"), long]]
    pub run_file_name: String,
    #[arg[default_value_t = String::from("config.ron"), long]]
    pub config_file_name: String,
    #[arg[long]]
    pub output: Option<PathBuf>,
    pub image_size: u32,
    pub t_start: usize,
    pub vector_length: u32
}

impl Default for Visualisation {
    fn default() -> Self {
        Self {
            input: PathBuf::from("~/vicsek/run"),
            data_file_name: String::from("data.json"),
            run_file_name: String::from("run.json"),
            config_file_name: String::from("config.ron"),
            output: None,
            image_size: 400,
            t_start: 0,
            vector_length: 5,
        }
    }
}

impl Visualisation {
    pub fn vis_dir(&self) -> PathBuf {
        match &self.output {
            None => {
                let path = PathBuf::from(&self.input).join("vis");
                if !path.exists() {
                    std::fs::create_dir(&path).unwrap();
                }
                path
            }
            Some(path) => { path.to_path_buf() }
        }
    }
}

impl Settings {
    pub fn new(config_file_path: &Path, config_file_prefix: &Path, local_config: &Path) -> Result<Self, ConfigError> {
        let run_mode = env::var("RUN_MODE").unwrap_or_else(|_| "development".into());
        let s = Config::builder()
            .add_source(config::File::new(config_file_path.to_str().unwrap(), config::FileFormat::Ron))
            .add_source(
                config::File::with_name(Path::new(&config_file_prefix).join(run_mode).to_str().expect("Can't read the Config File")).required(false))
            .add_source(config::File::new(config_file_prefix.join(local_config).to_str().expect("Can't read the Config File"), config::FileFormat::Ron).required(false))
            .add_source(Environment::with_prefix("app"))
            .build()?;

        s.try_deserialize()
    }

    pub fn new2(local_config: Option<PathBuf>) -> Self {
        local_config.map_or(Settings::default(), |path| {
            let f = File::open(path).unwrap();
            ron::de::from_reader(f).unwrap()
        })
    }

}

impl MultiVicsek {
    pub fn new(path: Option<PathBuf>) -> Self {
        path.map_or(MultiVicsek::from(Vicsek::default()), |path| {
            let f = File::open(path).unwrap();
            ron::de::from_reader(f).unwrap()
        })
    }
}

impl Vicsek {
    pub fn new(path: Option<PathBuf>) -> Self {
        path.map_or(Vicsek::default(), |path| {
            let f = File::open(path).unwrap();
            ron::de::from_reader(f).unwrap()
        })
    }
}

#[cfg(test)]
mod test {
    use std::fs::File;
    use std::iter::{once};
    use std::path::{Path, PathBuf};
    use ron::ser::PrettyConfig;
    use crate::settings::{MultiVicsek, Settings};
    use crate::vicsek::Vicsek;

    #[test]
    fn test_settings() {
        let f = File::open(Path::new("config/local.ron")).unwrap();
        let settings: Settings = ron::de::from_reader(f).unwrap();
        let vicsek = Vicsek::new(&settings.vicsek);
        println!("{:?}", vicsek.spins);

        let settings = Settings::new(Path::new("config/default.ron"), Path::new(""), Path::new("config/local.ron")).unwrap();
        println!("{:?}", settings.vicsek.group_alignments);
    }

    #[test]
    fn test_multi_config() {
        let config = crate::settings::Vicsek::new(Some(PathBuf::from("config/ordered-moving-through-rotation.ron")));

        let multi_config = MultiVicsek::from(config);

        let mut f = File::create("config/ordered-moving-through-rotation-new.ron").unwrap();
        ron::ser::to_writer_pretty(&mut f, &multi_config, PrettyConfig::default()).unwrap();
    }

    #[test]
    fn test_de_config() {
        let config = MultiVicsek::new(Some(PathBuf::from("config/align-disalign-no-mut-muli.ron")));

        for c in config {
            println!("groups: {:?}", c.groups);
        }
    }
}
