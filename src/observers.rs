use std::mem;
use std::ops::Deref;
use std::sync::{Arc};
use std::thread::JoinHandle;
use crossbeam::channel::TryRecvError;
use futures::future::{join_all};
use serde::{Deserialize, Serialize};
use timer::Guard;
use tokio::runtime::{Runtime};
use tokio::sync::broadcast::{Sender};
use tokio::sync::broadcast::error::RecvError;
use crate::observers::events::ObserverEvents;
use crate::observers::impl_traits::{SacredObserver, SyncObserver};
use crate::observers::mongodb::{SyncMongoObserver};

pub(crate) mod mongodb;
mod impl_traits;
pub(crate) mod events;

pub(crate) struct ObserverSpawner {
    handle: JoinHandle<()>
}

impl ObserverSpawner
{
    pub fn run<C, M, R, T, I, AM>(observer: T, tx: &Sender<Arc<T::Event>>, rt: Arc<Runtime>) -> Self
        where
            T: SacredObserver<Event = ObserverEvents<C, M, R, AM, I>> + Send + Sync + 'static,
            C: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            M: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            R: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            I: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            AM: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
    {

            let obs = Arc::new(observer);

            let mut rx = tx.subscribe();

        let result = std::thread::spawn(move || {
                rt.block_on(async move {
                    let mut set = Vec::new();
                    loop {
                        let message = rx.recv().await;
                        match message {
                            Ok(event) => {
                                let obs = obs.clone();
                                if let e @ (ObserverEvents::LogMetrics(_) | ObserverEvents::ArtifactAdded(_) | ObserverEvents::ResourceAdded(_)) = &*event {
                                    set.push(tokio::spawn(async move {
                                        obs.on(&event).await;
                                    }));
                                } else {
                                    let _ = obs.on(&event).await;
                                }
                            }
                            Err(error) => {
                                match error {
                                    RecvError::Closed => {
                                        // todo handle when channel got closed (eg. all senders are dropped)
                                        break;
                                    }
                                    RecvError::Lagged(_) => {
                                        // receiver lays behind
                                        println!("lag occurred");
                                    }
                                }
                            }
                        }
                    }
                    let joins = join_all(set);
                    let _ = joins.await;
                });
            });

        ObserverSpawner {
            handle: result
        }
    }

    pub fn join(self) -> Result<(), Box<(dyn std::any::Any + std::marker::Send + 'static)>> {
        self.handle.join()
    }
}

pub enum Observers<C, M, R, AM, I>
    where
        C: Default,
        M: Default,
        R: Default,
        AM: Default,
        I: Default
{
    // MongoDB(MongoObserver<C, M, R, AM, I>),
    SyncMongoDB(SyncMongoObserver<C, M, R, AM, I>)
}

impl <C, M, R, AM, I> Observers<C, M, R, AM, I>
    where
        C: Default + for<'a> Deserialize<'a> + Serialize + Clone + Send + Sync + Unpin + 'static,
        M: Default + for<'a> Deserialize<'a> + Serialize + Clone + Send + Sync + Unpin + 'static,
        R: Default + for<'a> Deserialize<'a> + Serialize + Clone + Send + Sync + Unpin + 'static,
        AM: Default + for<'a> Deserialize<'a> + Serialize + Clone + Send + Sync + Unpin + 'static,
        I: Default + for<'a> Deserialize<'a> + Serialize + Clone + Send + Sync + Unpin + 'static
{
    pub(crate) fn start(self, broadcast_receiver: crossbeam::channel::Receiver<Arc<ObserverEvents<C, M, R, AM, I>>>) -> Spawner {
        match self {
            Observers::SyncMongoDB(observer) => {
                Spawner::Sync(SyncObserverSpawner::run(observer, broadcast_receiver))
            }
        }
    }
}

pub (crate) enum Spawner {
    Sync(SyncObserverSpawner),
    Async(ObserverSpawner)
}

impl Spawner {
    pub(crate) fn join(self) -> Result<(), Box<(dyn std::any::Any + std::marker::Send + 'static)>> {
        match self {
            Spawner::Sync(obs) => { obs.join() }
            Spawner::Async(obs) => { obs.join() }
        }
    }
}

pub(crate) struct SyncObserverSpawner {
    handle: JoinHandle<()>
}

impl  SyncObserverSpawner
{
    pub fn run<C, M, R, T, I, AM>(observer: T, rx: crossbeam::channel::Receiver<Arc<T::Event>>) -> Self
        where
            T: SyncObserver<Event = ObserverEvents<C, M, R, AM, I>> + Send + Sync + 'static,
            C: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            M: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            R: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            I: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
            AM: Clone + Serialize + for<'a> Deserialize<'a> + Sync + Send + 'static,
    {

        let obs = Arc::new(observer);

        let result = std::thread::spawn(move || {
                loop {
                    let message = rx.try_recv();
                    match message {
                        Ok(event) => {
                            let obs = obs.clone();
                            let _ = obs.on(&event);
                        }
                        Err(error) => {
                            match error {
                                TryRecvError::Empty => {}
                                TryRecvError::Disconnected => {
                                    // todo() channel closed eg. all senders are dropped
                                    break;
                                }
                            }
                        }
                    }
                }
        });

        SyncObserverSpawner {
            handle: result,
        }
    }

    pub fn join(self) -> Result<(), Box<(dyn std::any::Any + std::marker::Send + 'static)>> {
        self.handle.join()
    }
}
