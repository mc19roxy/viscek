use std::path::{PathBuf};
use chrono::{DateTime, Utc};
use fxhash::FxHashSet;
use mongodb::bson::oid::ObjectId;
use crate::io::{Metric, MetricsByName, RunStatus};
use serde::{Serialize, Deserialize};
use url::Url;

#[derive(Clone, Debug)]
pub enum ObserverEvents<C, M, R, AM, I>
    where
        C: Clone + Serialize + for<'a> Deserialize<'a>,
        M: Clone + Serialize + for<'a> Deserialize<'a>,
        R: Clone + Serialize + for<'a> Deserialize<'a>,
        AM: Clone + Serialize + for<'a> Deserialize<'a>,
        I: Clone + Serialize + for<'a> Deserialize<'a>
{
    Queued(Queued<C, M>),
    Started(Started<C, M>),
    Heartbeat(Heartbeat<R, I>),
    Completed(Completed<R>),
    Interrupted(Interupted),
    Failed(Failed),
    ResourceAdded(ResourceAdded),
    ArtifactAdded(ArtifactAdded<AM>),
    LogMetrics(LogMetrics<I>),
    LogData(LogData)
}

#[derive(Clone, Debug)]
pub struct Queued<C, M> {
    pub(crate) ex_info: ExperimentInfo,
    pub(crate) command: String,
    pub(crate) host_info: HostInfo,
    pub(crate) config: C,
    pub(crate) meta_info: M,
    pub(crate) _id: Option<i64>,
}

#[derive(Clone, Debug)]
pub struct Started<C, M> {
    pub(crate) ex_info: ExperimentInfo,
    pub(crate) command: String,
    pub(crate) host_info: HostInfo,
    pub(crate) start_time: DateTime<Utc>,
    pub(crate) config: C,
    pub(crate) meta_info: M,
    pub(crate) sender: Option<crossbeam::channel::Sender<i64>>,
    pub(crate) _id: Option<i64>
}

#[derive(Clone, Debug)]
pub struct Heartbeat<R, I> {
    pub(crate) captured_out: String,
    pub(crate) beat_time: DateTime<Utc>,
    pub(crate) result: R,
    pub(crate) info: I
}

#[derive(Clone, Debug)]
pub struct Completed<R> {
    pub(crate) stop_time: DateTime<Utc>,
    pub(crate) result: R
}

#[derive(Clone, Debug)]
pub struct Interupted {
    pub(crate) stop_time: DateTime<Utc>,
    pub(crate) status: RunStatus
}

#[derive(Clone, Debug)]
pub struct Failed{
    pub(crate) fail_time: DateTime<Utc>,
    pub(crate) fail_trace: String
}

#[derive(Clone, Debug)]
pub struct ResourceAdded {
    pub(crate) file_name: String
}

#[derive(Clone, Debug)]
pub struct ArtifactAdded<AM> {
    pub(crate) name: String,
    pub(crate) data: Vec<u8>,
    pub(crate) metadata: AM,
    pub(crate) content_type: String
}

#[derive(Clone, Debug)]
pub struct LogMetrics<I> {
    pub(crate) metrics: MetricsByName,
    pub(crate) info: I
}

#[derive(Clone, Debug)]
pub struct LogData {
    pub(crate) data: Vec<Metric>

}

#[derive(Serialize, Deserialize, Clone, Default, Debug)]
pub struct HostInfo {
    pub(crate) host: Option<String>,
    pub(crate) rust_version: Option<String>,
    pub(crate) os: Option<String>,
    pub(crate) cpu: Option<String>,
    pub(crate) gpu: Option<String>
}

#[derive(Serialize, Deserialize, Clone, Default, Debug)]
pub struct ExperimentInfo {
    pub(crate) name: String,
    pub(crate) base_dir: Option<PathBuf>,
    pub(crate) sources: Option<FxHashSet<SourceFile>>,
    pub(crate) dependencies: Option<FxHashSet<Dependency>>,
    pub(crate) repositories: Option<Vec<Repository>>,
    pub(crate) main_file: Option<String>
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct MetricMetadata {
    pub(crate) name: String,
    pub(crate) id: ObjectId
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Info<I> {
    pub metrics: Vec<MetricMetadata>,
    pub run_data: Vec<MetricMetadata>,
    #[serde(flatten)]
    pub info: I
}

#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Clone, Default, Debug)]
pub struct SourceFile {
    name: String,
}

#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Clone, Default, Debug)]
pub struct Dependency {
    name: String,
    version: String
}

#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Clone, Default, Debug)]
pub struct Repository {
    pub url: String,
    pub commit: String,
    pub dirty: bool
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct Result{}
