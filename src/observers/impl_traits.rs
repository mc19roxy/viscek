use async_trait::async_trait;

pub trait Observer {
    type Event: Clone + Sync + Send;

    fn on(&mut self, event: &Self::Event);
}

#[async_trait]
pub trait SacredObserver {
    type Event: Clone + Sync + Send;

    async fn on(&self, event: &Self::Event);
}

pub trait SyncObserver {
    type Event: Clone + Sync + Send;

    fn on(&self, event: &Self::Event);
}