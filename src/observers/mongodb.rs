use std::io::Write;
use std::iter::zip;
use std::marker::PhantomData;
use std::path::PathBuf;
use std::sync::Arc;
use std::sync::mpsc::Sender;
use std::thread;
use std::time::Duration;
use mongodb::bson::{doc, Document, to_bson};
use mongodb::error::ErrorKind;
use mongodb::options::{CountOptions, FindOptions, GridFsUploadOptions, InsertManyOptions, InsertOneOptions, UpdateOptions};
use crate::io::{Artifact, RunEntry, RunStatus};
use crate::observers::events::{Info, MetricMetadata, ObserverEvents};
use crate::observers::impl_traits::{SyncObserver};
use mongodb::results::InsertOneResult;
use serde::{Deserialize, Serialize};
use serde::de::DeserializeOwned;

pub struct SyncMongoObserver<C, M, R, AM, I>
    where
        C: Default,
        M: Default,
        R: Default,
        I: Default,
        AM: Default
{
    client: mongodb::sync::Client,
    database: mongodb::sync::Database,
    run_collection: mongodb::sync::Collection<RunEntry<C, M, R, I>>,
    metrics_collection: mongodb::sync::Collection<Document>,
    data_collection: mongodb::sync::Collection<Document>,
    fs: mongodb::sync::gridfs::GridFsBucket,
    run_entry: std::sync::RwLock<Option<RunEntry<C, M, R, I>>>,
    failure_dir: PathBuf,
    metrics_metadata: std::sync::RwLock<Vec<MetricMetadata>>,
    run_data_metadata: std::sync::RwLock<Vec<MetricMetadata>>,
    phantom: PhantomData<AM>,
}

impl <C, M, R, AM, I> SyncMongoObserver<C, M, R, AM, I>
    where
        C: Serialize + DeserializeOwned + Sync + Send + Unpin + Default + Clone + Unpin,
        M: Serialize + DeserializeOwned + Sync + Send + Unpin + Default + Clone + Unpin,
        R: Serialize + DeserializeOwned + Sync + Send + Unpin + Default + Clone + Unpin,
        I: Serialize + DeserializeOwned + Sync + Send + Unpin + Default + Clone + Unpin,
        AM: Default + Unpin
{
    pub fn builder<'a>() -> SyncMongoObserverBuilder<'a> {
        SyncMongoObserverBuilder::default()
    }

    fn insert_run(&self)  -> Result<InsertOneResult, mongodb::error::Error> {
        let autoinc_key = {
            self.run_entry.read().as_ref().unwrap().as_ref().unwrap()._id.is_none()
        };

        loop {
            if autoinc_key {
                let find_options = FindOptions::builder().sort(doc! {"_id": -1}).limit(1).build();
                let mut cursor = self.run_collection.find(doc! {}, find_options)?;

                let id = match self.run_collection.count_documents(doc! {}, CountOptions::builder().limit(1).build())? {
                    0 => { Some(1) },
                    _ => {
                        let id = cursor.next().unwrap()?._id.unwrap() + 1i64;
                        Some(id)
                    }
                };

                {
                    self.run_entry.write().as_mut().unwrap().as_mut().unwrap()._id = id;
                }

            }
            return self.run_collection.insert_one(self.run_entry.read().as_ref().unwrap().as_ref().unwrap(), InsertOneOptions::default());
        }
    }

    fn save(&self) {
        if let Some(run_entry) = self.run_entry.read().as_ref().unwrap().as_ref() {
            if let Some(_id) = run_entry._id {
                let result = self.run_collection.update_one(doc! {"_id": _id}, doc! {"$set": to_bson(run_entry).unwrap() }, None);
                match result {
                    Ok(_) => {

                    }
                    Err(error) => {
                        match *error.kind {
                            ErrorKind::InvalidArgument { message, .. } => {}
                            _ => {}
                        }
                    }
                }
            }
        }
    }

    fn final_save(&self, attempts: usize) {
        for i in 0..attempts {
            if let Some(run_entry) = self.run_entry.read().as_ref().unwrap().as_ref() {
                let result = self.run_collection.update_one(doc! {"_id": run_entry._id},
                                                            doc! {"$set": to_bson(run_entry).unwrap()},
                                                            UpdateOptions::builder().upsert(true).build());

                match result {
                    Ok(_) => { break; },
                    Err(error) => {
                        match *error.kind {
                            _ => {
                                // todo handle specific error kinds. For know we just sleep and try again
                                if i < attempts - 1 {
                                    thread::sleep(Duration::from_millis(1));
                                }
                            }
                        }
                    }
                }
            }
        }

        let r = std::fs::create_dir_all(&self.failure_dir);
        match r {
            Ok(_) => {}
            Err(_) => {
                // todo the sacred implementation uses tempfiles. There's a tempfile crate on cargo. In the future it may be replaced using that crate
                let r_lock = self.run_entry.read();
                let prefix = format!("sacred_mongo_fail_{}.json", r_lock.as_ref().unwrap().as_ref().unwrap()._id.unwrap());
                let dir = self.failure_dir.join(PathBuf::from(prefix));
                let result = std::fs::write(&dir, serde_json::to_string_pretty(r_lock.as_ref().unwrap().as_ref().unwrap()).unwrap());
                match result {
                    Ok(_) => {}
                    Err(_) => {}
                }
            }
        }


    }
}

impl <C, M, R, AM, I> SyncObserver for SyncMongoObserver<C, M, R, AM, I>
    where
        C: Clone + Serialize + for<'a> Deserialize<'a> + Send + Sync + Default + Unpin,
        M: Clone + Serialize + for<'a> Deserialize<'a> + Send + Sync + Default + Unpin,
        R: Clone + Serialize + for<'a> Deserialize<'a> + Send + Sync + Default + Unpin,
        I: Clone + Serialize + for<'a> Deserialize<'a> + Send + Sync + Default + Unpin,
        AM: Default + Clone + Serialize + for<'a> Deserialize<'a> + Send + Sync + Unpin
{
    type Event = ObserverEvents<C, M, R, AM, I>;

    fn on(&self, event: &Self::Event) {
        match event {
            ObserverEvents::Queued(_) => {}
            ObserverEvents::Started(started) => {
                let ex_info = started.ex_info.to_owned();
                let command = started.command.to_owned();
                let started_time = started.start_time;
                let host = started.host_info.to_owned();
                let config = started.config.to_owned();
                let meta_info = started.meta_info.to_owned();
                let _id = started._id;


                let run_entry = RunEntry::new_started(ex_info, command, started_time, config, host, meta_info);
                {
                    *self.run_entry.write().unwrap() = Some(run_entry);
                }

                self.insert_run().unwrap();

                if let Some(sender) = &started.sender {
                    sender.send(self.run_entry.read().as_ref().unwrap().as_ref().unwrap()._id.unwrap()).unwrap();
                }
            }
            ObserverEvents::Heartbeat(heartbeat) => {
                let beat_time = heartbeat.beat_time;
                let captured_out = heartbeat.captured_out.to_owned();
                let results = heartbeat.result.to_owned();
                let info = heartbeat.info.to_owned();
                let updated_info = Info {
                    metrics: self.metrics_metadata.read().unwrap().to_owned(),
                    run_data: self.run_data_metadata.read().unwrap().to_owned(),
                    info
                };

                if let Some(run_entry) = self.run_entry.write().as_mut().unwrap().as_mut() {
                    run_entry.heartbeat(beat_time).captured_out(captured_out).results(results).info(updated_info);
                }

                self.save();
            }
            ObserverEvents::Completed(completed) => {
                {
                    let mut w_lock = self.run_entry.write();
                    w_lock.as_mut().unwrap().as_mut().unwrap()
                        .stop_time(completed.stop_time)
                        .status(RunStatus::Completed)
                        .results(completed.result.clone());
                }
                self.final_save(10);
            }
            ObserverEvents::Interrupted(interrupted) => {
                {
                    let mut w_lock = self.run_entry.write();
                    w_lock.as_mut().unwrap().as_mut().unwrap()
                        .stop_time(interrupted.stop_time)
                        .status(interrupted.status.clone());
                }

                self.final_save(3);
            }
            ObserverEvents::Failed(failed) => {
                {
                    let mut w_lock = self.run_entry.write();
                    w_lock.unwrap().as_mut().unwrap()
                        .stop_time(failed.fail_time)
                        .status(RunStatus::Failed(failed.fail_trace.clone()));
                }

                self.final_save(1);
            }
            ObserverEvents::ResourceAdded(resource) => {
                let mut result = self.fs.find(doc! {"filename": &resource.file_name}, None).unwrap();
                if let Some(resource) = result.next() {
                    // todo calculate md5 hash and search for filename + hash value
                    let filename = resource.unwrap().filename;
                    if let Some(filename) = filename {
                        let contains = {
                            let r_lock = self.run_entry.read();
                            !r_lock.unwrap().as_ref().unwrap().resources().contains(&filename)
                        };

                        if !contains {
                            let mut w_lock = self.run_entry.write();
                            w_lock.unwrap().as_mut().unwrap().add_resource(filename);
                            self.save();
                        }
                    }

                }
            }
            ObserverEvents::ArtifactAdded(artifact_event) => {
                let id = {
                    let r_lock = self.run_entry.read().unwrap();
                    let run_entry = r_lock.as_ref().unwrap();
                    run_entry._id
                };

                let mut reader = artifact_event.data.as_slice();

                let db_filename = format!("artifact://{}/{}/{}", self.run_collection.name(), id.unwrap(), &artifact_event.name);

                let mut fs_upload_stream = self.fs.open_upload_stream(&db_filename, GridFsUploadOptions::builder().metadata(to_bson(&artifact_event.metadata).unwrap().as_document().cloned()).build());
                fs_upload_stream.write_all(&mut reader).unwrap();

                let file_id = fs_upload_stream.id().as_object_id().unwrap();

                let result = fs_upload_stream.close();

                match result {
                    Ok(_) => {
                        self.run_entry.write().unwrap().as_mut().unwrap().add_artifact(Artifact {name: artifact_event.name.to_owned(), file_id});
                        self.save();
                    }
                    Err(_) => {}
                }
            }
            ObserverEvents::LogMetrics(metrics) => {
                let mut futures = Vec::default();
                let mut keys = Vec::default();
                if let Some(run_entry) = self.run_entry.read().as_ref().unwrap().as_ref() {
                    let Some(id) = run_entry._id else { return };
                    for (key, metric) in &metrics.metrics {
                        let query = doc! {"run_id": id, "name": key};
                        let push = doc! {
                            "steps": {"$each": to_bson(&metric.steps).unwrap()},
                            "values": {"$each": to_bson(&metric.values).unwrap()},
                            "timestamps": {"$each": to_bson(&metric.timestamps).unwrap()}
                        };
                        let update = doc! {"$push": push};
                        futures.push(self.metrics_collection.update_one(query, update, UpdateOptions::builder().upsert(true).build()));
                        keys.push(key);
                    }
                }

                for (key, result) in zip(keys, futures.iter()) {
                    if let Ok(update_result) =  result {
                        if let Some(upserted_id) = &update_result.upserted_id {
                            let mut w_lock = self.metrics_metadata.write().unwrap();
                            let metrics: &mut Vec<MetricMetadata> = w_lock.as_mut();

                            metrics.push(MetricMetadata {
                                name: key.to_string(),
                                id: upserted_id.as_object_id().unwrap()
                            });
                        }
                    }
                }
            }
            ObserverEvents::LogData(data) => {
                let metrics = &data.data;
                if let Some(run_entry) = self.run_entry.read().unwrap().as_ref() {
                    let Some(id) = run_entry._id else { return };
                    let result = self.data_collection.insert_many(metrics.iter().map(|m| {
                        doc! {"run_id": id, "step": to_bson(&m.step).unwrap(), "value": to_bson(&m.value).unwrap(), "time_stamp": m.timestamp, "name": &m.name}
                    }), InsertManyOptions::builder().build());

                    match result {
                        Ok(r) => {
                            let mut w_lock = self.run_data_metadata.write().unwrap();
                            let data: &mut Vec<MetricMetadata> = w_lock.as_mut();
                            for (metric, id) in zip(metrics.iter(), r.inserted_ids.values()) {
                                data.push(MetricMetadata {
                                    name: metric.name.to_string(),
                                    id: id.as_object_id().unwrap()
                                })
                            }
                        }
                        Err(_) => {}
                    }
                }
            }
        }
    }
}

#[derive(Default)]
pub struct SyncMongoObserverBuilder<'a> {
    client: Option<mongodb::sync::Client>,
    collection_prefix: Option<&'a str>,
    failure_dir: PathBuf,
    db_name: Option<String>
}

impl <'a> SyncMongoObserverBuilder<'a> {
    pub fn client(mut self, client: mongodb::sync::Client) -> Self {
        self.client = Some(client);
        self
    }

    pub fn collection_prefix(mut self, prefix: &'a str) -> Self {
        self.collection_prefix = Some(prefix);
        self
    }

    pub fn failure_dir(mut self, failure_dir: PathBuf) -> Self {
        self.failure_dir = failure_dir;
        self
    }

    pub fn db_name(mut self, db_name: String) -> Self {
        self.db_name = Some(db_name);
        self
    }

    pub fn build<C, M, R, AM, I>(self) -> SyncMongoObserver<C, M, R, AM, I>
        where
            C: Default,
            M: Default,
            R: Default,
            I: Default,
            AM: Default
    {
        // let options = handle.block_on(ClientOptions::parse("mongodb://localhost:27017")).unwrap();
        let client = self.client.unwrap_or(mongodb::sync::Client::with_uri_str("mongodb://localhost:27017").unwrap());
        let database = client.database(&self.db_name.unwrap_or("sacred".to_string()));
        let (runs_collection_name, metrics_collection_name) = if let Some(prefix) = self.collection_prefix {
            (format!("{}_runs", prefix), format!("{}_metrics", prefix))
        } else {
            (String::from("runs"), String::from("metrics"))
        };


        SyncMongoObserver {
            client,
            run_collection: database.collection(&runs_collection_name),
            metrics_collection: database.collection(&metrics_collection_name),
            data_collection: database.collection("data"),
            fs: database.gridfs_bucket(None),
            run_entry: std::sync::RwLock::new(Option::default()),
            database,
            failure_dir: self.failure_dir,
            metrics_metadata: Default::default(),
            run_data_metadata: Default::default(),
            phantom: PhantomData,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use std::sync::Arc;
    use std::sync::mpsc::{Receiver, sync_channel, SyncSender};
    use chrono::Utc;
    use serde::{Deserialize, Serialize};
    use tokio::runtime::Builder;
    use crate::observers::events::{ExperimentInfo, HostInfo, MetricMetadata, ObserverEvents, Queued, Started};
    use crate::observers::ObserverSpawner;
    use crate::settings::{UpdateStrategy, Vicsek};

    #[derive(Default, Serialize, Deserialize, Clone, Debug)]
    struct Config {
        a: usize,
        groups: HashMap<i8, (usize, Option<UpdateStrategy>)>,
    }

    #[derive(Default, Serialize, Deserialize, Clone, Debug)]
    struct MetaData {
        header: String
    }

    #[derive(Default, Serialize, Deserialize, Clone, Debug)]
    struct Result {
        res: bool
    }

    #[derive(Default, Serialize, Deserialize, Clone, Debug)]
    struct Info {}

    #[derive(Default, Serialize, Deserialize, Clone, Debug)]
    struct ArtifactMetaData {}
}

